﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using SSqlTranslator.Language.Interpreter;
using SSqlTranslator.Language.Parser;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Reader;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Console
{
	public class SsqlExecutor
	{
		public void Run()
		{
			System.Console.WriteLine("Введите имя файла со скриптом. ");
			System.Console.WriteLine("Пустая строка - файл '.\\script.ssql': ");
			System.Console.Write("--> ");
			string path = System.Console.ReadLine();
			if (path.Length == 0)
				path = ".\\script.ssql";

			try
			{
				ISourceCodeReader reader = new FileReader();
				ITokenizer scaner = new Scaner(reader, path);
				scaner.Scan();
				System.Console.WriteLine("Лексический анализ завершен успешно...\n\n");

				IParser parser = new RecursiveDescentParser(scaner);
				parser.Parse();
				WriteRpnToFile(parser.Commands, parser.State.Labels);
				System.Console.WriteLine("Синтаксический анализ завершен успешно...\n\n");
				
				var commands = parser.Commands.Select(c => c.Value);
				Interpreter interpreter = new Interpreter(commands, parser.State.Labels);
				System.Console.WriteLine("Вывод скрипта:\n");
				interpreter.ExecuteProgram();
				System.Console.WriteLine("Выполнение программы завершено успешно...\n\n");
				
				System.Console.WriteLine("Вывести БД?  [Y/-Y]: ");
				System.Console.Write("--> ");
				string ans = System.Console.ReadLine();
				if (ans.ToUpper() == "Y" || ans.Length == 0)
					interpreter.PrintDataBase();
			}
			catch (Exception e)
			{
				System.Console.WriteLine("ERROR: " + e.Message);
			}
                
			System.Console.Write("Press any key to continue . . . ");
			System.Console.ReadKey(true);
		}
		
		private void WriteRpnToFile(IEnumerable<Command> rpn, Dictionary<int, int> labels)
		{
			using (FileStream fs = new FileStream(".\\program.rpn", FileMode.Create))
			{
				using (StreamWriter file = new StreamWriter(fs, Encoding.UTF8))
				{
					file.WriteLine(rpn.Count());
					foreach (Command s in rpn)
					{
						file.WriteLine(s.Value);
					}
		            
					file.WriteLine(labels.Count);
					foreach (int labelKey in labels.Keys)
					{
						file.WriteLine(labelKey + " " + labels[labelKey]);
					}
				}
			}
		}
	}
}
