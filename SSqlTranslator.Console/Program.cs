﻿using System;

namespace SSqlTranslator.Console
{
	class Program
	{
		public static void Main(string[] args)
		{
			SsqlExecutor program = new SsqlExecutor();
			program.Run();
		}
	}
}