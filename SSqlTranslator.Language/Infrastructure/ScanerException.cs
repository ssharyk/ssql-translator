﻿using System;
using System.Runtime.Serialization;

namespace SSqlTranslator.Language.Infrastructure
{
	/// <summary>
	/// The exception that is throw when making tokens structure of the script fails
	/// </summary>
	public class ScanerException : Exception, ISerializable
	{
		/// <summary>
		/// Gets the error reason
		/// </summary>
		public ScanerExceptionReason Reason { get; private set; }

		public ScanerException(ScanerExceptionReason reason)
			: this(reason, reason.ToString())
		{
		}

		public ScanerException(ScanerExceptionReason reason, string message) : this(reason, message, null)
		{
		}

		public ScanerException(ScanerExceptionReason reason, string message, Exception innerException) : base(message, innerException)
		{
			this.Reason = reason;
		}

		// This constructor is needed for serialization.
		protected ScanerException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}

	public enum ScanerExceptionReason
	{
		InvalidSource,
		InvalidTransactionBounds,

		InfiniteComment,
		InfiniteStringLiteral,

		InvalidComparison,
		UnknownSymbol,
		InvalidLiteral,
		MismatchOperatorsNumber
	}
}