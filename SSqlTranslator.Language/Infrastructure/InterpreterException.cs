﻿using System;
using System.Runtime.Serialization;

namespace SSqlTranslator.Language.Infrastructure
{
	/// <summary>
	/// The exception that is throw when making tokens structure of the script fails
	/// </summary>
	public class InterpreterException : Exception, ISerializable
	{
		/// <summary>
		/// Gets the error reason
		/// </summary>
		public InterpreterExceptionReason Reason { get; private set; }

		public InterpreterException(InterpreterExceptionReason reason)
			: this(reason, reason.ToString())
		{
		}

		public InterpreterException(InterpreterExceptionReason reason, string message) : this(reason, message, null)
		{
		}

		public InterpreterException(InterpreterExceptionReason reason, string message, Exception innerException) : base(message, innerException)
		{
			this.Reason = reason;
		}

		// This constructor is needed for serialization.
		protected InterpreterException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}

	public enum InterpreterExceptionReason
	{
		MathError,
		ZeroDivision,
		Nan,
		
		UncompatibleDataTypes,
		UncompatibleStringOperation,
		
		TableNotCreated,
		TableAlreadyExists,
		UncompatibleFieldsInsertion,
		MigrationFailure,
		PrimaryKeyUniqueFailure,
		LinkCreationFail,
		
		UndefinedVariable,
		NoVariable,
		NoField
	}
}