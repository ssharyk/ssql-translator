﻿using System;
using System.Runtime.Serialization;

namespace SSqlTranslator.Language.Infrastructure
{
	/// <summary>
	/// The exception that is throw when defining identifiers and/or their values fails
	/// </summary>
	public class IdentifierException : Exception, ISerializable
	{
		/// <summary>
		/// Gets the error reason
		/// </summary>
		public IdentifierExceptionReason Reason { get; private set; }

		public IdentifierException(IdentifierExceptionReason reason)
			: this(reason, reason.ToString())
		{
		}

		public IdentifierException(IdentifierExceptionReason reason, string message) : this(reason, message, null)
		{
		}

		public IdentifierException(IdentifierExceptionReason reason, string message, Exception innerException) : base(message, innerException)
		{
			this.Reason = reason;
		}

		// This constructor is needed for serialization.
		protected IdentifierException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}

	public enum IdentifierExceptionReason
	{
		UnknownSymbol,
		IdentifierTypeNotSpecified,
		ConstantOverriding,
		DelimerterRequired
	}
}