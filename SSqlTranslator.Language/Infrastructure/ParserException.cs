﻿using System;
using System.Runtime.Serialization;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Infrastructure
{
	/// <summary>
	/// The exception that is throw when making tokens structure of the script fails
	/// </summary>
	public class ParserException : Exception, ISerializable
	{
		/// <summary>
		/// Gets the error reason
		/// </summary>
		public ParserExceptionReason Reason { get; private set; }

		public ParserException(ParserExceptionReason reason)
			: this(reason, reason.ToString())
		{
		}

		public ParserException(ParserExceptionReason reason, string message) : this(reason, message, null)
		{
		}

		public ParserException(ParserExceptionReason reason, string message, Exception innerException) : base(message, innerException)
		{
			this.Reason = reason;
		}

		// This constructor is needed for serialization.
		protected ParserException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>
		/// Handles situatuion when found token is not the ine expected
		/// </summary>
		/// <param name="token">Invalid token</param>
		/// <param name="message">Additional error message</param>
		public static void HandleRequirement(Lexeme token, string message)
		{
			throw new ParserException(ParserExceptionReason.TokenRequired, message);
		}
	}

	public enum ParserExceptionReason
	{
		InvalidLexemeIndex,
		EndOfProgram,

		InvalidAssignment,
		ConstantReAssignment,

		TokenRequired
	}
}