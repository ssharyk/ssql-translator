﻿using System;

namespace SSqlTranslator.Language.Interpreter.DatabaseEntities
{
	public class Link
	{
		public Table PK_Table { get; set; }
        public Field PK_Field { get; set; }

        public Table FK_Table { get; set; }
        public Field FK_Field { get; set; }

        public Link(Table pkT, Field pkF, Table fkT, Field fkF)
        {
            PK_Table = pkT;
            PK_Field = pkF;

            FK_Table = fkT;
            FK_Field = fkF;
        }

        public override string ToString()
        {
            return "[Link: " + PK_Table.Name + " ! " + PK_Field.Name +
             "    <--->    " + FK_Table.Name + " ! " + FK_Field.Name + "]";
        }
	}
}
