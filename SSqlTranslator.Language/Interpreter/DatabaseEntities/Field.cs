﻿using System;

namespace SSqlTranslator.Language.Interpreter.DatabaseEntities
{
	public class Field
	{
        public int Number { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public bool IsPK { get; set; }

        public double MaxValue { get; set; }
        public double MinValue { get; set; }
        public double AvgValue { get; set; }

        public Field(int n, string dt, string nm, bool isPK)
        {
            Number = n;
            Type = dt;
            Name = nm;
            IsPK = isPK;

            ClearValues();
        }

        public void ClearValues()
        {
            MaxValue = Double.MinValue;
            MinValue = Double.MaxValue;
            AvgValue = 0;
        }

        public override string ToString()
        {
            return Number + " " + Name + " --> " + MinValue;
        }
	}
}
