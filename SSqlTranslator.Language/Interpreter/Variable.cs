﻿using System;

namespace SSqlTranslator.Language.Interpreter
{
	public class Variable
	{
		public string DataType { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsConst { get; set; }

        public Variable(string dt, string nm, string val, bool isC)
        {
            DataType = dt;
            Name = nm;
            Value = val;
            IsConst = isC;
        }
	}
}
