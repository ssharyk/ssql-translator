﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Interpreter.DatabaseEntities;
using SSqlTranslator.Language.Interpreter.Query;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Interpreter
{
	public class Interpreter
	{
        #region Поля и свойства
        
        public List<string> PostfixForm { get; protected set; }
        public List<Variable> Identifiers { get; protected set; }
        protected Dictionary<int, int> _labels;

        protected Stack<string> _stack;
        protected int _currentElement;
        public List<Table> Tables { get; protected set; }
        public List<Link> Links { get; protected set; }
        private Stack<QueryInfo> _queries;

        private static string[] _operators = {   "+", "-", "*", "/", "%", "^",
                                                 "==", "<>", "><", ">=", "<=", "<", ">", 
                                                 "И", "ИЛИ" };
        private static string[] _operatorsUnar = { "НЕ", "УПЛ", "БП", "$LENGTH",
                                                   "НАИБ", "НАИМ", "СРЕДНЕЕ" };
        private static string[] _commands = { "ВЫВОД", ":", "СОЗДАТЬ_Т", "СОЗДАТЬ_К", "СОЗДАТЬ_С", "УДАЛИТЬ_Т", "ВСТАВИТЬ", "ВЫБРАТЬ", "ЕСЛИ" };
        
        #endregion

        #region Конструирование
        
        public Interpreter(IEnumerable<string> rpn, Dictionary<int, int> labels)
        {
        	PostfixForm = new List<string>(rpn);
        	_labels = new Dictionary<int, int>(labels);
            _stack = new Stack<string>();

            Identifiers = new List<Variable>();
            Tables = new List<Table>();
            Links = new List<Link>();
            _queries = new Stack<QueryInfo>();
        }

        private void ReadFromFile()
        {
            Console.WriteLine("Введите имя для файла ОПЗ: ");
            Console.WriteLine("По умолчанию: ..\\..\\..\\program.rpn");
            Console.Write("--> ");
            //string path = Console.ReadLine();
            string path = ".\\parser.rpn";
            if (path.Length == 0)
                path = "..\\..\\..\\program.rpn";
            StreamReader file = new StreamReader(new FileStream(path, FileMode.Open));

            int pfL = Int32.Parse(file.ReadLine());
            PostfixForm = new List<string>(pfL);
            for (int i = 0; i < pfL; i++)
            {
                PostfixForm.Add(file.ReadLine());
            }

            int lblL = Int32.Parse(file.ReadLine());
            _labels = new Dictionary<int, int>();
            for (int l = 0; l < lblL; l++)
            {
                string[] s = file.ReadLine().Split(' ');
                _labels[Int32.Parse(s[0])] = Int32.Parse(s[1]);
            }

            file.Close();
        }
        
        #endregion

        #region ВЫПОЛНЕНИЕ

        public void ExecuteProgram()
        {
            string s;
            for (_currentElement = 0; _currentElement < PostfixForm.Count; _currentElement++)
            {
                s = PostfixForm.ElementAt(_currentElement);

                if (s == "$EOP")
                {
                    return;
                }

                if (_operators.Contains(s))
                {
                    string op1 = _stack.Pop(), op2 = _stack.Pop();
                    string res = ApplyOperation(s, op2, op1);
                    _stack.Push(res);

                    continue;
                }

                if (_operatorsUnar.Contains(s))
                {
                    string op1 = _stack.Pop();
                    string res = ApplyOperation(s, op1);
                    _stack.Push(res);

                    continue;
                }

                if (_commands.Contains(s))
                {
                    switch (s)
                    {
                        case ":":
                            {
                                string op1 = _stack.Pop(), op2 = _stack.Pop();
                                ExecuteAssignment(op2, op1);
                                break;
                            }

                        case "СОЗДАТЬ_Т":
                            {
                                ExecuteTableCreation();
                                break;
                            }

                        case "СОЗДАТЬ_К":
                            {
                                ExecutePKCreation();
                                break;
                            }

                        case "СОЗДАТЬ_С":
                            {
                                ExecuteLinkCreation();
                                break;
                            }
                        case "УДАЛИТЬ_Т":
                            {
                                ExecuteTableDelete();
                                break;
                            }

                        case "ВСТАВИТЬ":
                            {
                                ExecuteInsertion();
                                _stack.Pop();
                                break;
                            }

                        case "ВЫВОД":
                            {
                                ExecutePrint();
                                break;
                            }
                    }

                    continue;
                }

                // точка входа в запрос
                if (s.IndexOf("_ITERATOR_") != -1 &&
                    PostfixForm.ElementAt(_currentElement + 1) == "0" &&
                    PostfixForm.ElementAt(_currentElement + 2) == ":")
                {
                    QueryInfo info = new QueryInfo();
                    string tblName = s.Substring(0, s.IndexOf("_"));
                    info.CurrentTable = GetTableByName(tblName);
                    if (info.CurrentTable == null)
                        throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, "Таблица " + tblName + " еще не создана");

                    info.IsInQuery = Int32.Parse(s.Substring(s.LastIndexOf("_") + 1));
                    info.QueryParameters = new List<QueryParameter>();
                    foreach (Field field in info.CurrentTable.Fields)
                        field.ClearValues();
                    string resName = info.CurrentTable.Name;
                    info.ResultTable = new Table(resName);

                    _queries.Push(info);

                    if (info.IsInQuery == 1)
                    {
                        InitQuerySelect();
                        if (_queries.Peek().CurrentTable.Name[0] == '$')
                            _queries.Peek().ResultTable.Name = _queries.Peek().CurrentTable.Name;
                        else
                            _queries.Peek().ResultTable.Name = "$" + _queries.Peek().CurrentTable.Name;

                        PrepareOutputTable();
                        _stack.Push(info.CurrentTable.Name + "_ITERATOR_" + info.IsInQuery);
                        continue;
                        //_iterator--;                        
                    }
                    else
                    {
                        InitQueryModify();
                        if (info.IsQueryFinish)
                        {
                            info.IsQueryFinish = false;
                            _queries.Pop();
                            continue;
                        }
                        PrepareOutputTable();
                    }
                }

                if (s.IndexOf("_ITERATOR_") != -1 &&
                    PostfixForm[_currentElement + 1].IndexOf("_ITERATOR_") != -1 &&
                    PostfixForm[_currentElement + 2] == "1" &&
                    PostfixForm[_currentElement + 3] == "+" &&
                    PostfixForm[_currentElement + 4] == ":")
                {
                    _queries.Peek().Iterator++;
                    _currentElement += 4;
                    _stack.Push(_queries.Peek().CurrentTable.Name + "_ITERATOR_" + _queries.Peek().IsInQuery);
                    continue;
                }

                _stack.Push(s);
            }
        }

        #region Вычисления
        
        private string ApplyOperation(string zn, string op1, string op2)
        {
            string o1 = GetValue(op1);
            string o2 = GetValue(op2);

            string dt1 = GetValueType(o1), dt2 = GetValueType(o2);
            if (dt1 == dt2)
            {
                switch (dt1)
                {
                    case "ЛОГ":
                        {
                            bool v1 = op1 == "ИСТИНА";
                            bool v2 = op2 == "ИСТИНА";

                            if (zn == "И")
                            {
                                return ((v1 && v2) ? "ИСТИНА" : "ЛОЖЬ");
                            }

                            if (zn == "ИЛИ")
                            {
                                return ((v1 || v2) ? "ИСТИНА" : "ЛОЖЬ");
                            }
                            throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно применить операцию " + zn + " к логическим значениям");
                        }

                    case "СТР":
                        {
                            if (zn == "==") return (o1 == o2) ? "ИСТИНА" : "ЛОЖЬ";
                            if (zn == "<>") return (o1 != o2) ? "ИСТИНА" : "ЛОЖЬ";
                            if (zn == "+") return o1.Substring(0, o1.Length-1) + o2.Substring(1);                            
                            throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно применить операцию " + zn + " к строковым значениям");
                        }

                    case "ВЕЩ":
                        {
                            double d1 = -11.1, d2 = -22.2;
                            try
                            {
                                o1 = o1.Replace('.', ',');
                                o2 = o2.Replace('.', ',');

                                d1 = double.Parse(o1);
                                d2 = double.Parse(o2);
                            }
                            catch (FormatException)
                            {
                                throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно преобразование типов для того, чтобы выполнить операцию " + zn);
                            }

                            switch (zn)
                            {
                                case "+":
                                    {
                                        return (d1 + d2).ToString();
                                    }

                                case "-":
                                    {
                                        return (d1 - d2).ToString();
                                    }

                                case "*":
                                    {
                                        return (d1 * d2).ToString();
                                    }
                                case "/":
                                    {
                                        if (Math.Abs(d2) < 0.0000001)
                                            throw new InterpreterException(InterpreterExceptionReason.ZeroDivision, "Деление на 0");

                                        if ((d1 / d2).ToString() == "NaN")
                                            throw new InterpreterException(InterpreterExceptionReason.Nan, "Получена неопределенность при делении " + d1 + " / " + d2);
                                        return (d1 / d2).ToString();
                                    }

                                case "%":
                                    {
                                        if (Math.Abs(d2) < 0.0000001)
                                            throw new InterpreterException(InterpreterExceptionReason.ZeroDivision, "Деление на 0");

                                        if ((d1 / d2).ToString() == "NaN")
                                            throw new InterpreterException(InterpreterExceptionReason.Nan, "Получена неопределенность при делении " + d1 + " / " + d2);

                                        return (d1 % d2).ToString();
                                    }

                                case "^":
                                    {
                                        try
                                        {
                                            return Math.Pow(d1, d2).ToString();
                                        }
                                        catch (Exception)
                                        {
                                            throw new InterpreterException(InterpreterExceptionReason.MathError, "Не удалось выполнить операцию возведения в степень");
                                        }
                                    }


                                case ">":
                                    {
                                        return (d1 > d2) ? "ИСТИНА" : "ЛОЖЬ";
                                    }
                                case ">=":
                                    {
                                        return (d1 >= d2) ? "ИСТИНА" : "ЛОЖЬ";
                                    }
                                case "<":
                                    {
                                        return (d1 < d2) ? "ИСТИНА" : "ЛОЖЬ";
                                    }
                                case "<=":
                                    {
                                        return (d1 <= d2) ? "ИСТИНА" : "ЛОЖЬ";
                                    }
                                case "<>":
                                    {
                                        return (d1 != d2) ? "ИСТИНА" : "ЛОЖЬ";
                                    }
                                case "==":
                                    {
                                        return (d1 == d2) ? "ИСТИНА" : "ЛОЖЬ";
                                    }
                            }

                            throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно применить операцию " + zn + " к вещественным значениям");
                        }
                }
            }

            if (GetValueType(o1) == "СТР" && GetValueType(o2) == "ВЕЩ" && zn == "*")
            {
                uint count;
                if (!UInt32.TryParse(o2, out count))
                    throw new InterpreterException(InterpreterExceptionReason.UncompatibleStringOperation, "Невозможно повторить строку " + o1 + " " + o2 + " раз");
                
                string s = "";
                for (uint x = 0; x < count; x++)
                    s += o1.Substring(1, o1.Length - 2);
                return "\"" + s + "\"";
            }
            throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Операнды " + op1 + " и " + op2 + " разных типов при применении " + zn);
        }

        private string ApplyOperation(string zn, string op)
        {
            //Console.WriteLine("\tApply {0}({1})", zn, op);
            switch (zn)
            {
                case "НЕ":
                    {
                        string o = GetValue(op);
                        string dt = GetValueType(o);
                        if (dt == "ЛОГ")
                        {
                            if (o == "ИСТИНА") return "ЛОЖЬ";
                            if (o == "ЛОЖЬ") return "ИСТИНА";
                            throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно применить НЕ к выражению нелогического типа: " + o);
                        }
                        throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно применить НЕ к выражению нелогического типа: " + o);
                    }

                case "БП":
                    {
                        int lbl = Int32.Parse(op.Substring(1));
                        _currentElement = _labels[lbl] - 1;
                        return PostfixForm.ElementAt(_currentElement);
                    }

                case "УПЛ":
                    {
                        int lbl = Int32.Parse(op.Substring(1));

                        if (_stack.Peek() == "ЛОЖЬ")
                        {
                            for (int j0 = _currentElement + 1; j0 < _labels[lbl] - 1; j0++)
                            {
                                string s1 = PostfixForm.ElementAt(_currentElement);
                                if (!_commands.Contains(s1) && !_operators.Contains(s1) && !_operatorsUnar.Contains(s1))
                                    _stack.Push(s1);
                            }
                            _currentElement = _labels[lbl] - 1;
                        }
                        else
                        {
                            if (_queries.Count > 0 && PostfixForm[_currentElement - 3] != "$LENGTH")
                            {
                                switch (_queries.Peek().IsInQuery)
                                {
                                    case 1:
                                        {
                                            // в случае цикла ДЛЯ и вложенных @
                                            int j = PostfixForm.LastIndexOf("УПЛ", _currentElement - 1);
                                            if (PostfixForm[j - 3] == "$LENGTH")
                                                PrintPerformedRecord();
                                            break;
                                        }

                                    case 2: { _queries.Peek().IsNeedPrintRecord = true; break; }
                                    case 3:
                                        {
                                            //Console.WriteLine("delete with _iterator = " + _queries.Peek().Iterator);

                                            /// TODO: вернуть
                                            PrintPerformedRecord();

                                            Table table = _queries.Peek().CurrentTable;
                                            //table.Records.RemoveAt(_queries.Peek().Iterator);
                                            table.Records[_queries.Peek().Iterator] = null;
                                            break;
                                        }
                                }

                            }
                        }

                        break;
                    }

                case "$LENGTH":
                    {
                        //_isInQuery = 1; /// TOOD: 1-select, 2-modif
                        _stack.Push(PostfixForm[_currentElement - 1]);
                        //string tblName = PostfixForm.ElementAt(_currentElement - 1);
                        //tblName = tblName.Substring(0, tblName.IndexOf("_ITERATOR_"));

                        //Table table = GetTableByName(tblName);
                        Table table = _queries.Peek().CurrentTable;

                        if (_queries.Peek().IsInQuery != 0 && _queries.Peek().Iterator < table.Counter)
                        {
                            //Console.WriteLine("_iterator = {0}", _iterator);
                            if (_queries.Peek().IsInQuery == 1)
                            {
                                InitQuerySelect();
                            }
                            else
                            {
                                //Console.WriteLine("MODIFICATION:");
                                UpdateParameters();
                                //Console.WriteLine("\r\n\r\n");
                            }
                        }
                        else
                        {
                            string queryType = "";
                            QueryInfo info = _queries.Pop();
                            switch (info.IsInQuery)
                            {
                                case 1: { queryType = "выборку"; break; }
                                case 2: { queryType = "модификацию"; break; }
                                case 3: { queryType = "удаление"; break; }                            
                            }
                            /// TODO: вернуть
                            //info.ResultTable.toPrint();
                            Console.WriteLine("Всего обработано строк в этом запросе на {0}: {1}\r\n\r\n",
                                queryType, info.NumberOfPerformedRecords);                            

                            if (info.IsInQuery == 1)
                            {
                                // вывод полей под агрегатными функциями
                                bool isAgg = false;
                                for (int ai = _currentElement + 2; ai < info.QueryBody; ai++)
                                {
                                    if (PostfixForm[ai] == "НАИБ" || PostfixForm[ai] == "НАИМ" || PostfixForm[ai] == "СРЕДНЕЕ")
                                    {
                                        isAgg = true;
                                        string fldName = PostfixForm[ai - 1];
                                        Table tbl=GetTableByName( PostfixForm[ai - 2]);                                       
                                        Field field = tbl.GetFieldByName(fldName);

                                        string v = "";
                                        if (PostfixForm[ai] == "СРЕДНЕЕ")
                                            v = field.AvgValue.ToString();
                                        else
                                            v = (PostfixForm[ai] == "НАИБ") ? field.MaxValue.ToString() : field.MinValue.ToString();

                                        Console.WriteLine("Значение {0}({1}) = {2}", PostfixForm[ai], fldName, v);
                                    }
                                }

                                // удаление таблицы-произведения
                                for (int jt = 0; jt < Tables.Count; jt++)
                                {
                                    if (Tables[jt].Name[0] == '$')
                                    {
                                        Tables.RemoveAt(jt);
                                        break;
                                    }
                                }
                                if (isAgg) Console.WriteLine("\r\n\r\n");
                            }

                            for (int jd = 0; jd < table.Records.Count; jd++)
                            {
                                if (table.Records[jd] == null)
                                {
                                    table.Records.RemoveAt(jd);
                                    jd--;
                                }
                            }
                            table.Counter = table.Records.Count;

                            int lblN = Int32.Parse(PostfixForm[_currentElement + 2].Substring(1));
                            _currentElement = _labels[lblN];

                            return PostfixForm[_currentElement];
                        }

                        //Console.WriteLine("Length of {0} is {1}    ITER: {2}", tblName, table.Records.Count, _iterator);
                        return table.Records.Count.ToString();
                    }

                case "НАИБ":
                case "НАИМ":
                case "СРЕДНЕЕ":
                    {
                        //Table table = _queries.Peek().CurrentTable;
                        string tblName = PostfixForm[_currentElement - 2];
                        Table table = GetTableByName(tblName);

                        string fldName = op;
                        Field field = table.GetFieldByName(fldName);
                        if (field == null)
                            throw new InterpreterException(InterpreterExceptionReason.NoField, "Поля " + fldName + " не существует в таблице " + table.Name);
                        int fldNumber = field.Number;

                        //Console.WriteLine("!!!!!!!!!!!!! aggr: {0} ( {1}: {2} ).", zn, fldNumber, fldName);

                        string aggr = _queries.Peek().CurrentTable.ApplyAggregate(fldNumber, zn, _queries.Peek().Iterator, _queries.Peek().NumberOfPerformedRecords - 1, table);
                        //Console.WriteLine("\tNow aggr = " + aggr);

                        return aggr;
                    }
            }
            return "";
        }
        
        #endregion

        #region Выполнение команд
        
        private void ExecuteAssignment(string to, string from)
        {
            // UPDATE table SET field = value
            if (_queries.Count > 0 && _queries.Peek().IsInQuery == 2 && to.IndexOf("_ITERATOR_") == -1)
            {
                // запись в таблицу
                Table table = _queries.Peek().CurrentTable;
                int num = table.GetFieldNumber(to);


                string valUpd = from;
                if (_queries.Count > 1 && from[0] == '_' && from[1] != '_')
                {
                    // UPDATE внутри FOR-EACH
                    Stack<QueryInfo> stack = new Stack<QueryInfo>(_queries);
                    QueryInfo qi = stack.Pop();
                    QueryParameter par = null;
                    do
                    {
                        par = qi.toGetParameterByVariableName(from);
                        if (par != null)
                        {
                            valUpd = par.FieldValue;
                        }
                        qi = stack.Pop();
                    } while (stack.Count > 0);

                    if (qi == null)
                        throw new InterpreterException(InterpreterExceptionReason.NoVariable, "Такой переменной нет");
                }

                table.Records[_queries.Peek().Iterator][num] = valUpd;

                // обновление списка пар-ров
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    if (par.FieldName == to)
                    {
                        par.FieldValue = valUpd;
                        break;
                    }
                }

                /// TODO: вернуть
                PrintPerformedRecord();

                return;
            }

            Variable var = GetVariableByName(to);
            string val = GetValue(from);
            if (var == null)
            {
            	string dt = DataTypes.ДИН.ToString();
            	try {
            		dt = _stack.Pop();
            		Enum.Parse(typeof(DataTypes), dt);
            	} catch(InvalidOperationException) {
            	} catch(ArgumentException) {
            	}
            	
                if (dt == DataTypes.ДИН.ToString() ||
                    GetValueType(val) == dt)
                {
                    var = new Variable(dt, to, val, to[1] == '_');
                    Identifiers.Add(var);
                }
                else
                {
                    if (to[0] != '_')
                    {
                        string tblName = to;
                        Table table = GetTableByName(tblName);
                        if (table == null)
                            throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, "Таблицу " + tblName + " нельзя использовать в запросе, так как ее еще не существует");

                        string fldName = from;
                    }
                    else
                    {
                        throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Несоответствие типов данных при присваивании: невозможно присвоить " + dt + " " + to + " значение " + GetValueType(val) + " " + from);
                    }
                }
            }
            else
            {
            	if (var.DataType == DataTypes.ДИН.ToString() ||
	           	    GetValueType(val) == var.DataType)
                {
                    var.Value = val;
                }
                else
                {
                    throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Невозможно приведение типов при присваивании значения " + val + " в переменную " + to + " типа " + var.DataType);
                }
            }
        }

        private void ExecuteTableCreation()
        {
            int n = Int32.Parse(_stack.Pop()) - 2;
            string tblName = _stack.ElementAt(n + 1);
            Table test = GetTableByName(tblName);
            if (test != null)
                throw new InterpreterException(InterpreterExceptionReason.TableAlreadyExists, "Таблица " + tblName + " уже существует");

            Table tbl = new Table(tblName);

            for (int i = 0; i < n; i += 2)
            {
                string nm = _stack.ElementAt(n - (i + 1));
                string dt = _stack.ElementAt(n - (i + 0));
                tbl.AddField(dt, nm);
            }
            for (int i = 0; i < n + 2; i++)
            {
                _stack.Pop();
            }
            Tables.Add(tbl);
        }

        private void ExecutePKCreation()
        {
            string tblName = _stack.Pop();
            Table table = GetTableByName(tblName);
            if (table == null)
            {
                string message = "Таблицы " + tblName + " еще не существует!\n";
                message += "Создайте ее, прежде чем обрабатывать поля.";
                throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, message);
            }

            int parameters = Int32.Parse(_stack.Pop());
            for (int i = 0; i < parameters; i++)
            {
                string fld = _stack.Pop();
                table.MarkPK(fld);
            }
        }

        private void ExecuteInsertion()
        {
            int n = Int32.Parse(_stack.Pop());
            string tbl = _stack.ElementAt(n - 1);
            Table table = GetTableByName(tbl);
            if (table == null)
                throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, "Таблица " + tbl + " еще не создана");

            if (n - 1 != table.Fields.Count)
                throw new InterpreterException(InterpreterExceptionReason.UncompatibleFieldsInsertion, "Несоответствие количества полей в таблице и вставляемых записей");

            // вставка
            int lines = table.Records.Count;
            table.Records.Add(new Dictionary<int, string>());
            bool isNotUnique = true;
            string key = "]";

            for (int i = n - 2; i >= 0; i--)
            {
                // проверка типов данных
                string val = _stack.Pop();
                Field fld = table.Fields.ElementAt(i);
                string dt = GetValueType(val);
                if (dt != fld.Type && (!(dt == "ВЕЩ" && fld.Type == "ЦЕЛ")))
                    throw new InterpreterException(InterpreterExceptionReason.UncompatibleDataTypes, "Несоответствие типов данных значения " +dt+" "+ val + " и поля " + fld.Type + " " + fld.Name);

                // проверка нарушения PK
                //bool isNU = table.isNotUnique(i, val);
                //isNotUnique &= isNU;
                //if (isNU) key = (" " + val + " ") + key;

                if (!IsMigrationCorrect(table, fld, val))
                    throw new InterpreterException(InterpreterExceptionReason.MigrationFailure, "Нельзя вставить запись со значением поля " + fld.Name +
                        " = " + val + ", так как нарушается миграция ключей");

                table.Records.ElementAt(lines)[i] = val;
            }

            isNotUnique = table.IsNotUnique();

            if (isNotUnique)
            {
                key = table.GetPK(table.Records.Count - 1);
                isNotUnique = false;
                throw new InterpreterException(InterpreterExceptionReason.PrimaryKeyUniqueFailure, "Запись с ключом " + key + " уже существует в таблице " + table.Name);
            }

            table.Counter++;
        }

        private void ExecutePrint()
        {
            int parameters = Int32.Parse(_stack.Pop());
            Console.Write(">>> ");

            for (int i = parameters - 1; i >= 0; i--)
            {
                string op = _stack.ElementAt(i);
                string s = GetValue(op);
                if (s[0] == '"')
                {
                    s = s.Substring(1, s.Length - 2);
                }
                Console.Write(s);
            }
            Console.WriteLine();
        }

        private void ExecuteTableDelete()
        {
            string tblname = _stack.Pop();
            Table table = GetTableByName(tblname);
            if (table == null)
                throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, "Таблицы " + tblname + " не существует");
            Tables.Remove(table);
        }

        #region Связи
        
        private void ExecuteLinkCreation()
        {
            string fkF = _stack.Pop();
            string fkT = _stack.Pop();
            string pkF = _stack.Pop();
            string pkT = _stack.Pop();
            string link_str = pkT + " ! " + pkF + "    <--->    " + fkT + " ! " + fkF;

            // вторичный ключ
            Table tableF = GetTableByName(fkT);
            if (tableF == null)
                throw new InterpreterException(InterpreterExceptionReason.LinkCreationFail, "Невозможно создать связь " + link_str + " так как таблицы " + fkT + " еще не существует");
            Field fieldF = tableF.GetFieldByName(fkF);
            if (fieldF == null)
                throw new InterpreterException(InterpreterExceptionReason.LinkCreationFail, "Невозможно создать связь " + link_str + " так как поля " + fkF + " еще не существует в таблице " + fkT);

            // первичный ключ
            Table tableP = GetTableByName(pkT);
            if (tableP == null)
                throw new InterpreterException(InterpreterExceptionReason.LinkCreationFail, "Невозможно создать связь " + link_str + " так как таблицы " + pkT + " еще не существует");
            Field fieldP = tableP.GetFieldByName(pkF);
            if (fieldP == null)
                throw new InterpreterException(InterpreterExceptionReason.LinkCreationFail, "Невозможно создать связь " + link_str + " так как поля " + pkF + " еще не существует в таблице " + pkT);

            Links.Add(new Link(tableP, fieldP, tableF, fieldF));

            // 1) список допустимых значений вторичного ключа
            List<string> allowed = GetAllowedValues(tableP, fieldP.Number);

            // 2) проверка миграции
            foreach (Dictionary<int, string> rec in tableF.Records)
            {
                if (!allowed.Contains(rec[fieldF.Number]))
                    throw new InterpreterException(InterpreterExceptionReason.LinkCreationFail, "Ошибка миграции ключа: нельзя создать связь" +
                        " так как в поле " + tableP.Name + " ! " + fieldP.Name + " нет ключа " + rec[fieldF.Number]);
            }
        }

        private bool IsMigrationCorrect(Table table, Field field, string val)
        {
            foreach (Link link in Links)
            {
                if (link.FK_Table == table && link.FK_Field == field)
                {
                    int fldNumber = link.PK_Field.Number;
                    List<string> allowed = GetAllowedValues(link.PK_Table, fldNumber);
                    if (!allowed.Contains(val)) return false;
                }
            }
            return true;
        }

        private List<string> GetAllowedValues(Table table, int fldNumber)
        {
            List<string> allowed = new List<string>();
            foreach (Dictionary<int, string> rec in table.Records)
            {
                allowed.Add(rec[fldNumber]);
            }
            return allowed;
        }
        
        #endregion

        #region Запрос
        
        private void InitQuerySelect()
        {
            int cur = 0;
            int i = _currentElement;
            //_iterator=0;
            _queries.Peek().QueryParameters.Clear();

            string[] aggF = { "НАИБ", "НАИМ", "СРЕДНЕЕ" };
            // переход на начало заполнения
            for (; i < PostfixForm.Count; i++)
            {
                if (PostfixForm[i][0] == '@')
                {
                    cur++;
                    if (cur == 2)
                    {
                        break;
                    }
                }
            }
            i += 2;

            string v_nm, f_nm, zn, tbl;
            List<Table> usedTables = new List<Table>();
            for (int j = i; j < PostfixForm.Count; j += 5)
            {
                zn = PostfixForm[j + 4];

                if (zn != ":" && !aggF.Contains(zn)) //|| !Char.IsLetter(PostfixForm[j+1][0]))
                {
                    _queries.Peek().QueryBody = j;
                    break;
                }

                if (zn == ":")
                {
                    if (PostfixForm[j].IndexOf("_ITERATOR_") != -1)
                    {
                        _queries.Peek().QueryBody = j;
                        break;
                    }

                    v_nm = PostfixForm[j + 1];
                    tbl = PostfixForm[j + 2];
                    Table table = GetTableByName(tbl);
                    if (table == null)
                        throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, "Таблицы " + tbl + " еще не существует");
                    if (!usedTables.Contains(table))
                        usedTables.Add(table);
                    f_nm = PostfixForm[j + 3];
                    _queries.Peek().QueryParameters.Add(new QueryParameter(table, v_nm, f_nm, GetFieldValue(tbl, f_nm), false));
                }
                if (aggF.Contains(zn)) j++;
            }


            if (usedTables.Count > 1)
            {
                /// TODO: вложенные запросы => в них тоже поменяется, но этого не надо
                for (int j = _currentElement; j < _queries.Peek().QueryBody + 5; j++)
                {
                    foreach (Table table in usedTables)
                    {
                        if (table.Name == PostfixForm[j] && !aggF.Contains(PostfixForm[j+2]))
                            PostfixForm[j] = "$1";

                        int ind = PostfixForm[j].IndexOf("_ITERATOR_");
                        if (ind != -1)
                        {
                            string t = PostfixForm[j].Substring(0, ind);
                            if (table.Name == t)
                                PostfixForm[j] = "$1" + "_ITERATOR_" + "1";
                        }
                    }
                }

                Table join = new Table("$1");
                // поля
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    Field field = par.Table.GetFieldByName(par.FieldName);
                    join.AddField(field.Type, par.FieldName);
                }

                // записи
                int numberOfRepeat = 1;
                foreach (Table table in usedTables)
                    numberOfRepeat *= table.Records.Count;
                join.Records = new List<Dictionary<int, string>>(numberOfRepeat);
                join.Counter = numberOfRepeat;
                for (int j = 0; j < numberOfRepeat; j++)
                    join.Records.Add(new Dictionary<int, string>());

                int incr = 1;
                foreach (Table table in usedTables)
                {
                    numberOfRepeat /= table.Records.Count;

                    foreach (QueryParameter par in _queries.Peek().QueryParameters)
                    {
                        if (par.Table == table)
                        {
                            int fldNumberIn = table.GetFieldNumber(par.FieldName);
                            int fldNumberOut = join.GetFieldNumber(par.FieldName);

                            // запись в исходной таблице
                            int line = 1 - incr;
                            for (int iter = 0; iter < table.Records.Count; iter++)
                            {
                                string val = table.Records[iter][fldNumberIn];

                                for (int times = 0; times < numberOfRepeat; times++)
                                {
                                    line += incr;
                                    if (incr > 1 && line > join.Records.Count)
                                        line = iter + 1;

                                    join.Records[line - 1][fldNumberOut] = val;
                                }
                            }
                        }
                    }

                    incr *= numberOfRepeat;
                    numberOfRepeat *= table.Records.Count;
                }


                Tables.Add(join);
                _queries.Peek().CurrentTable = join;
            }
        }

        private void PrintPerformedRecord()
        {
            _queries.Peek().NumberOfPerformedRecords++;
            _queries.Peek().ResultTable.Records.Add(new Dictionary<int,string>());

            Console.Write("{0,2}", _queries.Peek().NumberOfPerformedRecords);
            foreach (QueryParameter par in _queries.Peek().QueryParameters)
            {
                Console.Write("|{0,15}", par.FieldValue);
                int fldNumber=_queries.Peek().ResultTable.GetFieldNumber(par.FieldName);
                _queries.Peek().ResultTable.Records[_queries.Peek().NumberOfPerformedRecords-1][fldNumber] = par.FieldValue;
            }
            Console.WriteLine();
        }

        private void InitQueryModify()
        {
            // условие модификации
            int i = _currentElement;
            if (_queries.Peek().CurrentTable.Records.Count == 0)
            {
                Console.WriteLine("В этом запросе таблица пуста\r\n\r\n");
                _queries.Peek().IsQueryFinish = true;

                int lbl = Int32.Parse(PostfixForm[_currentElement + 6].Substring(1));
                _currentElement = _labels[lbl] - 1;
                _queries.Peek().QueryBody = PostfixForm.Count;

                return;
            }

            // переход на начало заполнения
            for (; i < PostfixForm.Count; i++)
            {
                if (PostfixForm[i][0] == '@')
                {
                    break;
                }
            }
            i += 1;

            do
            {
                i++;
                string s = PostfixForm[i];
                if (Char.IsLetter(s[0]) && s.IndexOf("_ITERATOR_") == -1)
                {
                    _queries.Peek().QueryParameters.Add(new QueryParameter(_queries.Peek().CurrentTable, "_" + s, s, GetFieldValue(_queries.Peek().CurrentTable.Name, s), false));
                }

                if (s[0] == '@')
                {
                    if (PostfixForm[i + 1] == "БП") break;
                    i++;
                }
            } while (true);
            _queries.Peek().QueryBody = PostfixForm.Count;


            // удаление всех записей из таблицы
            if (_queries.Peek().IsInQuery == 3 && _queries.Peek().QueryParameters.Count == 0)
            {
                _queries.Peek().IsQueryFinish = true;
                // очистка таблицы
                Table table = _queries.Peek().CurrentTable;

                int n = table.Records.Count;
                table.Records.RemoveRange(0, n);
                Console.WriteLine("Всего обработано строк в этом запросе на удаление (очистка таблицы): {0}\r\n\r\n", n);

                int lbl = Int32.Parse(PostfixForm[_currentElement + 6].Substring(1));
                _currentElement = _labels[lbl] - 1;

                // переход на конец запроса
                _queries.Peek().QueryBody = PostfixForm.Count;
            }
        }

        private void UpdateParameters()
        {
            Table table = _queries.Peek().CurrentTable;

            foreach (QueryParameter par in _queries.Peek().QueryParameters)
            {
                int num = table.GetFieldNumber(par.FieldName);
                par.FieldValue = table.Records[_queries.Peek().Iterator][num];
            }
        }
        
        #endregion
        
        #endregion
        
        #endregion

        #region Поиск значения переменной: Обращение к таблицам идентификаторов | полям таблиц БД
        
        private string GetValue(string query)
        {
            if (Char.IsDigit(query[0]) || query[0] == '"' ||
               query[0] == '+' || query[0] == '-' ||
               query == "ИСТИНА" || query == "ЛОЖЬ")
            {
                return query;
            }

            if (query.IndexOf("_ITERATOR_") != -1)
            {
                Variable v = GetVariableByName(query);
                if (v == null)
                    throw new InterpreterException(InterpreterExceptionReason.NoVariable, "Переменная " + query + " еще не описана");
                return v.Value;
            }


            if (_queries.Count > 0 && _queries.Peek().IsInQuery == 1 && query[0] == '_')
            {
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    if (par.VariableName == query)
                    {
                        return par.FieldValue;
                    }
                }
            }

            if (_queries.Count > 0 && _queries.Peek().IsInQuery != 0 && Char.IsLetter(query[0]))
            {
                foreach (QueryParameter par in _queries.Peek().QueryParameters)
                {
                    if (par.FieldName == query)
                    {
                        return par.FieldValue;
                    }
                }
            }

            if (query[0] == '_' || query.IndexOf("_ITERATOR_") != -1)
            {
                Variable v = GetVariableByName(query);
                if (v == null)
                    throw new InterpreterException(InterpreterExceptionReason.NoVariable, "Переменная " + query + " еще не описана");
                return v.Value;
            }

            return "";
        }

        private Variable GetVariableByName(string name)
        {
            foreach (Variable v in Identifiers)
            {
                if (v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }

        private string GetValueType(string val)
        {
        	if (val.Length == 0) return DataTypes.ДИН.ToString();
                //throw new InterpreterException(InterpreterExceptionReason.UndefinedVariable, "Значение переменной неопределено.\r\nВозможно, тип переменной не соответствует значению");

            if (val.IndexOf("_ITERATOR_") != -1) return "ВЕЩ";
            if (val == "ИСТИНА" || val == "ЛОЖЬ") return "ЛОГ";
            if (val[0] == '"' && val[val.Length - 1] == '"') return "СТР";
            if (Char.IsDigit(val[0]) || val[0] == '+' || val[0] == '-') return "ВЕЩ";

            throw new InterpreterException(InterpreterExceptionReason.UndefinedVariable, "Тип значения " + val + " не определен");
        }

        private Table GetTableByName(string name)
        {
            foreach (Table table in Tables)
            {
                if (table.Name == name)
                    return table;
            }
            return null;
        }

        private string GetFieldValue(string tblName, string fldName)
        {
            Table table = GetTableByName(tblName);
            if (table == null)
                throw new InterpreterException(InterpreterExceptionReason.TableNotCreated, "Такой таблицы еще не существует");

            Field field = table.GetFieldByName(fldName);
            if (field == null)
                throw new InterpreterException(InterpreterExceptionReason.NoField, "Поля " + fldName + " не существует в таблице " + tblName);

            return table.Records[_queries.Peek().Iterator][field.Number];
        }
        
        #endregion

        #region ОТЛАДКА
        
        private void PrintPostfixForm()
        {
            Console.WriteLine("\r\n\r\nPOSTFIX FORM:");
            for (int i = 0; i < PostfixForm.Count; i++)
            {
                Console.WriteLine("\t{0} --> {1}", i, PostfixForm.ElementAt(i));
            }
            Console.WriteLine("# POSTFIX FORM\r\n\r\n");
        }

        public void PrintIdTable()
        {
            Console.WriteLine("\r\n\r\nID/CONST:");
            foreach (Variable v in Identifiers)
            {
                Console.WriteLine("\t" + v);
            }
            Console.WriteLine("# ID/CONST\r\n\r\n");
        }

        private void PrintStack()
        {
            Console.WriteLine("STACK:");
            Stack<string> stack = new Stack<string>(_stack);
            while (stack.Count > 0)
            {
                Console.WriteLine("\t" + stack.Pop());
            }
            Console.WriteLine("# STACK");
        }

        private void PrintLabels()
        {
            Console.WriteLine("\r\n\r\nLABELS:");
            for (int i = 0; i < _labels.Keys.Count; i++)
            {
                Console.WriteLine("\t<{0}> --> <{1}>", _labels.Keys.ElementAt(i), _labels[_labels.Keys.ElementAt(i)]);
            }
            Console.WriteLine("# LABELS\r\n\r\n");
        }

        public void PrintDataBase()
        {
            Console.WriteLine("\n================");
            Console.WriteLine("БАЗА ДАННЫХ");
            Console.WriteLine("================");

            Console.WriteLine("\tВсего таблиц: " + Tables.Count);
            foreach (Table table in Tables)
            {
                table.toPrint();
            }

            Console.WriteLine("\r\n\r\n\tВсего связей: " + Links.Count);
            foreach (Link link in Links)
            {
                Console.WriteLine(link);
            }
            Console.WriteLine("================\n\n");
        }

        private void PrepareOutputTable()
        {
            // подготовка таблицы для вывода
            Console.Write("\r\n##");
            int n = 0;
            foreach (QueryParameter par in _queries.Peek().QueryParameters)
            {
                _queries.Peek().ResultTable.AddField(GetValueType(par.FieldValue), par.FieldName);
                if (!par.IsAggregate)
                {
                    Console.Write("|{0,15}", par.FieldName);
                    n++;
                }
            }
            Console.WriteLine();
            for (int parC = 0; parC < n; parC++)
                for (int l = 0; l < 15; l++)
                    Console.Write("-");
            Console.WriteLine("----");
        }
        
        #endregion
	}
}
