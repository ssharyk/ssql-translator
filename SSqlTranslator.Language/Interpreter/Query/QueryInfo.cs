﻿using System;
using System.Collections.Generic;
using SSqlTranslator.Language.Interpreter.DatabaseEntities;

namespace SSqlTranslator.Language.Interpreter.Query
{
	public class QueryInfo
	{
		public int Iterator { get; set; }
        public int IsInQuery { get; set; }
        public Table CurrentTable { get; set; }
        public int NumberOfPerformedRecords { get; set; }
        public bool IsNeedPrintRecord { get; set; }
        public bool IsQueryFinish { get; set; }
        public int QueryBody { get; set; }
        public List<QueryParameter> QueryParameters { get; set; }
        public Table ResultTable { get; set; }

        public QueryParameter toGetParameterByVariableName(string varName)
        {
            foreach (QueryParameter par in QueryParameters)
            {
                if (par.VariableName == varName)
                    return par;
            }
            return null;
        }
	}
}
