﻿using System;
using SSqlTranslator.Language.Interpreter.DatabaseEntities;

namespace SSqlTranslator.Language.Interpreter.Query
{
	public class QueryParameter
	{
		public Table Table { get; set; }
        public string VariableName { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public bool IsAggregate { get; set; }

        public QueryParameter(Table table, string v_nm, string f_nm, string v, bool aggr)
        {
            Table = table;
            VariableName = v_nm;
            FieldName = f_nm;
            FieldValue = v;
            IsAggregate = aggr;
        }
	}
}
