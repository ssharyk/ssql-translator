﻿using System;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Enumerates possible types of data in SSQL script
	/// </summary>
	public enum DataTypes
    {
		ДИН,
        ВЕЩ,
        ЛОГ,
        СТР
    }

	static class DataTypesExtensions
	{
		/// <summary>
		/// Safely parses string value of type
		/// </summary>
		/// <param name="l">String representation of the enum value</param>
		/// <param name="dt">Result of parsing or 'СТР' for default/error</param>
		/// <returns>true if result is found and saved in <paramref name="dt"/>; false otherwise</returns>
		public static bool TryParse(string l, out DataTypes dt)
        {
            dt = DataTypes.СТР;
            if (l == "ВЕЩ") { dt = DataTypes.ВЕЩ; return true; }
            if (l == "ЛОГ") { dt = DataTypes.ЛОГ; return true; }
            if (l == "СТР") { dt = DataTypes.СТР; return true; }
            return false;
        }

		public static DataTypes Parse(string l)
		{
			DataTypes dt = (DataTypes) Enum.Parse(typeof(DataTypes), l);
			return dt;
		}
	}
}
