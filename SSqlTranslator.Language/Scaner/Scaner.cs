﻿using System;
using System.Collections.Generic;
using System.Linq;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Reader;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Executes line-by-line scaning source code to build tokens collection
	/// </summary>
	public class Scaner : ITokenizer
	{
		#region Fields and properties

		private string _text;

		private int _currentProgramSymbolIndex;
		private int _programIndexToSkip;
		private ScanerState _current;

		private static readonly char[] Delimeters =
			{ '[', ']', '(', ')', ',', '!', ';', '*', '/', '%', '^' };
		private static readonly char[] UnimportantDelimeters =
			{ ' ', '\t', '\r', '\n' };
		private static readonly string[] Operations =
			{ "+", "-", "*", "/", "%" };
		private static readonly string[] KeyWords =
			Enum.GetNames(typeof(KeyWords));

		private IList<Lexeme> _lexemes;

		/// <summary>
		/// Gets collection of parsed lexemes
		/// </summary>
		public IEnumerable<Lexeme> Lexemes
		{
			get	{ return _lexemes; }
			private set { _lexemes = value.ToList(); }
		}

		private IList<Identifier> _identifiers;

		/// <summary>
		/// Gets collection of used identifiers (var / const)
		/// </summary>
		public IEnumerable<Identifier> Identifiers
		{
			get { return _identifiers; }
			private set { _identifiers = value.ToList(); }
		}

		#endregion

		#region Construction

		public Scaner(ISourceCodeReader reader, string source)
		{
			try
			{
				this._text = reader.Read(source);
			}
			catch(Exception e)
			{
				throw new ScanerException(ScanerExceptionReason.InvalidSource, "Cannot read from " + source, e);
			}

			this._lexemes = new List<Lexeme>();
			this.Identifiers = new List<Identifier>();
			this._current = new ScanerState
			{
				Line = 1
			};

			Identifier.Reset();
		}

		#endregion

		#region Main methods

		/// <summary>
		/// Executes tokenizing of the content
		/// </summary>
		/// <returns>Number of tokens found</returns>
		public int Scan()
		{
			MakeAnalysis();
			DefineTokensType();

			for (int i = 0; i < _lexemes.Count; i++)
            {
				Lexeme token = _lexemes[i];
				DefineIdentifier(i);
				NotifyUnresolvedSymbol(i);
			}

			return _lexemes.Count();
		}

		private void MakeAnalysis()
		{
			_text = _text.Trim();
            if (_text.Length < 2 ||_text[0] != '#' || _text[_text.Length - 1] != '#')
                throw new ScanerException(ScanerExceptionReason.InvalidTransactionBounds, "ошибка в ограничении транзакции #..#");
            _text = _text.Substring(1, _text.Length - 2).Replace("\t", "    ");

            for (_currentProgramSymbolIndex = 0; _currentProgramSymbolIndex < _text.Length; _currentProgramSymbolIndex++)
            {
                if (_programIndexToSkip > _currentProgramSymbolIndex)
                	continue;

                bool isComment = CheckComment();
                if (isComment)
                	continue;

                _current.Symbol = GetNextSymbol();
                if (UnimportantDelimeters.Contains(_current.Symbol))
                {
                	AddToken();
                	continue;
                }
               	if (Delimeters.Contains(_current.Symbol))
            	{
                	AddTokensAfterDelimeter();
                	continue;
                }
                switch(_current.Symbol)
                {
                	case '>':
                	case '<':
                	case '=':
                		AddComparisonTokens();
                		break;

                	case ':':
                		AddAssignmentTokens();
                		break;

                	case '+':
                	case '-':
                		AddBinaryOrUnarTokens();
                		break;

                	case '"':
                	case '.':
                		AddConstantTokens();
                		break;

                	default:
                		_current.Token += _current.Symbol;
                		break;
                }
            }

            if (_current.IsStringLiteral)
            	throw new ScanerException(ScanerExceptionReason.InfiniteStringLiteral, "незакрытая строковая константа");

            RemoveEmptyTokensCascade();
            ReInitTokensIdentifiers();
		}

		private void DefineTokensType()
		{
			int keyWordCode = -1;

			for (int i = 0; i < _lexemes.Count; i++)
			{
				Lexeme token = _lexemes.ElementAt(i);

				if (i > 0 && Operations.Contains(token.Value) &&
				    Operations.Contains(_lexemes[i - 1].Value))
					throw new ScanerException(ScanerExceptionReason.MismatchOperatorsNumber, "Следует задать два операнда для операции '" + _lexemes[i - 1].Value + "' в строке " + token.Line);

				if (token.Value == "!")
				{
					_lexemes[i - 1].Type = LexemeType.ИмяТаблицы;
					_lexemes[i + 1].Type = LexemeType.ИмяПоля;
				}

				if (token.Type != LexemeType.__)
					continue;

				if (KeyWords.Contains(token.Value))
				{
					token.Type = LexemeType.КлючевоеСлово;
					if (token.Value == "ТАБЛИЦУ" || token.Value == "КЛЮЧ" ||
					                   token.Value == "В" || token.Value == "ИЗ")
						keyWordCode = 1;
					if (token.Value == "СВЯЗЬ")
						keyWordCode = 3;
				}

				if (token.Value == ".")
				{
					token.Type = LexemeType.КонецВыражения;
					keyWordCode = -1;
				}
				if (token.Value == ":")
					token.Type = LexemeType.Присваивание;
				if (token.Value == "ИСТИНА" || token.Value == "ЛОЖЬ")
					token.Type = LexemeType.КонстантаЛОГ;
				if (token.Value[0] == '"')
					token.Type = LexemeType.КонстантаСТР;
				if (Char.IsDigit(token.Value[0]) || token.Value[0] == '-')
				{
					for (int j0 = 1; j0 < token.Value.Length; j0++)
					{
						if (token.Value[j0] != '.' && !Char.IsDigit(token.Value[j0]))
							throw new ScanerException(ScanerExceptionReason.InvalidLiteral, "неверная константа [" + token.Value + "]");
					}
					token.Type = LexemeType.КонстантаВЕЩ;
				}

				if (token.Value[0] == '_')
				{
					token.Type = (token.Value[1] == '_')
						? LexemeType.ИмяКонстанты : LexemeType.ИмяПеременной;
				}

				if (token.Type == LexemeType.__)
				{
					switch (keyWordCode)
					{
						case 1:
							{
								token.Type = LexemeType.ИмяТаблицы;
								if (_lexemes[i + 1].Value != ",")
									keyWordCode = 2;
								break;
							}
						case 2:
							{
								token.Type = LexemeType.ИмяПоля;
								break;
							}
						case 3:
							{
								token.Type = LexemeType.ИмяТаблицы;
								keyWordCode = 4;
								break;
							}
						case 4:
							{
								token.Type = LexemeType.ИмяПоля;
								keyWordCode = 3;
								break;
							}
						default:
							{
								throw new ScanerException(ScanerExceptionReason.UnknownSymbol, "Неизвестный символ: " + token.Value +
								" in " + token.Line + "; " + token.Symbol);
							}
					}
				}
			}
		}

		private void DefineIdentifier(int currentIndex)
		{
			Lexeme token = _lexemes[currentIndex];
            if (token.Type.ToString().StartsWith("Константа"))
            {
                String l = token?.Type.ToString().Substring("Константа".Length, 3);
                DataTypes dt = GetTypeForLexeme(l);
                Identifier registeredIdentifier = GetIdentifier(token.Value);
                if (registeredIdentifier == null)
                {
                	Identifier newIdentifier = new Identifier(null, dt, token.Value, true);
                    _identifiers.Add(newIdentifier);
                    token.IdentifierReference = newIdentifier.Number;
                }
                else
                {
                    token.IdentifierReference = registeredIdentifier.Number;
                }
            }

            if (token.Type == LexemeType.ИмяКонстанты)
            {
            	if (currentIndex == 0)
                {
                	throw new IdentifierException(
                		IdentifierExceptionReason.IdentifierTypeNotSpecified,
                		"Не указан тип данных для " + _lexemes[0]?.Value ?? "undefined");
                }
                Identifier registeredIdentifier = GetIdentifier(token.Value);
                if (registeredIdentifier == null)
                {
                	DataTypes dt = GetTypeForLexeme(_lexemes?[currentIndex - 1]?.Value);
                	Identifier newIdentifier = new Identifier(token.Value, dt, GetConstantValue(currentIndex), true);
                    _identifiers.Add(newIdentifier);
                    token.IdentifierReference = newIdentifier.Number;
                }
                else
                {
                	if (_lexemes[currentIndex + 1].Value == ":")
                        throw new IdentifierException(IdentifierExceptionReason.ConstantOverriding, "Константе не может быть присвоено новое значение");

                    token.IdentifierReference = registeredIdentifier.Number;
                }
            }

            if (token.Type == LexemeType.ИмяПеременной)
            {
                Identifier registeredIdentifier = GetIdentifier(token.Value);
                if (registeredIdentifier == null)
                {
                    string val = @"";
                    if (_lexemes[currentIndex + 1].Value != ":") val = "";
                    else val = GetConstantValue(currentIndex);

                    if (currentIndex == 0)
                    {
                    	throw new IdentifierException(
                    		IdentifierExceptionReason.IdentifierTypeNotSpecified,
                    		"Не указан тип данных для " + _lexemes[0]?.Value ?? "undefined");
                    }
                    DataTypes dt = GetTypeForLexeme(_lexemes?[currentIndex - 1]?.Value);
                    Identifier newIdentifier = new Identifier(token.Value, dt, val, false);
                    _identifiers.Add(newIdentifier);
                    token.IdentifierReference = newIdentifier.Number;
                }
                else
                {
                	if (_lexemes[currentIndex + 1].Value == ":")
                		_identifiers[registeredIdentifier.Number].Value = GetConstantValue(currentIndex);

                    token.IdentifierReference = registeredIdentifier.Number;
                }
            }
		}

		private void NotifyUnresolvedSymbol(int currentIndex)
		{
			Lexeme token = _lexemes[currentIndex];
			if (currentIndex > 0 &&
			    (token.Type.ToString().StartsWith("Имя") || token.Type.ToString().StartsWith("Константа")) &&
			    (_lexemes[currentIndex - 1].Type.ToString().StartsWith("Имя") ||
			     _lexemes[currentIndex - 1].Type.ToString().StartsWith("Константа")))
                    throw new IdentifierException(IdentifierExceptionReason.DelimerterRequired, "Требуется разделитель после '" + _lexemes[currentIndex-1].Value + "' в строке " + token.Line);


                if (token.Type == LexemeType.ИмяКонстанты || token.Type == LexemeType.ИмяПеременной ||
                    token.Type == LexemeType.ИмяПоля || token.Type == LexemeType.ИмяТаблицы)
                {
                    foreach (char c in token.Value)
                    {
                        if (c != '_' && !Char.IsLetterOrDigit(c))
                        	throw new IdentifierException(IdentifierExceptionReason.UnknownSymbol, "странный символ [" + c + "] в " + token.Value + "\nСтрока " + token.Line + " символ " + token.Symbol);
                    }
                }

                if (token.Type == LexemeType.__)
                	throw new IdentifierException(IdentifierExceptionReason.UnknownSymbol, "странный символ в " + token.Value);
		}

		#endregion

		#region Scaning support methods

		/// <summary>
		/// Checks for enclosing the line into string literal or single-/multi-line comment
		/// </summary>
		/// <returns><c>True</c> if comment was found</returns>
		private bool CheckComment()
		{
			if (_text[_currentProgramSymbolIndex] == '\"')
            {
				_current.IsStringLiteral = !_current.IsStringLiteral;
            }

			if (_text[_currentProgramSymbolIndex] == '?' && !_current.IsStringLiteral)
			{
				if (_text[_currentProgramSymbolIndex + 1] == '?')
				{
					_programIndexToSkip = _text.IndexOf("??", _currentProgramSymbolIndex + 1) + 1;
					if (_programIndexToSkip <= 0)
						throw new ScanerException(ScanerExceptionReason.InfiniteComment, "незакрытый комментарий (начало в строке " + _current.Line + ")");

					for (int j0 = _currentProgramSymbolIndex + 2; j0 < _programIndexToSkip; j0++)
					{
						_current.SymbolInLine++;
						if (_text[j0] == '\n')
						{
							_current.Line++;
							_current.SymbolInLine = 1;
						}
					}
				}
				else
				{
					_programIndexToSkip = _text.IndexOf("\n", _currentProgramSymbolIndex + 1);
					_current.SymbolInLine = 1;
				}
				return true;
			}
			return false;
		}

		/// <summary>
		/// Tracks line and symbol indicies according to current reading head position
		/// </summary>
		/// <returns>Current character under the reading head</returns>
		private char GetNextSymbol()
		{
			char c = _text[_currentProgramSymbolIndex];
            if (c == '\n')
            {
                _current.Line++;
                _current.SymbolInLine = 1;
            }
            else
            {
                _current.SymbolInLine++;
            }
            return c;
		}

		private void AddToken()
		{
			_lexemes.Add(new Lexeme(_current));
            _current.Token = @"";
		}

		private void AddTokensAfterDelimeter()
		{
            _lexemes.Add(new Lexeme(_current));
            _lexemes.Add(new Lexeme(LexemeType.Разделитель, _current.Symbol.ToString(), _current.Line, _current.SymbolInLine - 1));
            _current.Token = @"";
		}

		private void AddComparisonTokens()
		{
			if (_current.Symbol == '>' || _current.Symbol == '<')
            {
                _lexemes.Add(new Lexeme(_current));
                _current.Token = @"";
                string val = @"";
                if (_text[_currentProgramSymbolIndex + 1] == '=')
                {
                    val = _current.Symbol + "=";
                    _programIndexToSkip = _currentProgramSymbolIndex + 2;
                }
                else
                {
                	if ((_text[_currentProgramSymbolIndex + 1] == '>' && _current.Symbol == '<') ||
                	    (_text[_currentProgramSymbolIndex + 1] == '<' && _current.Symbol == '>'))
                    {
                        val = _current.Symbol + _text[_currentProgramSymbolIndex + 1].ToString();
                        _programIndexToSkip = _currentProgramSymbolIndex + 2;
                    }
                    else
                    {
                        val = _current.Symbol.ToString();
                        _programIndexToSkip = _currentProgramSymbolIndex + 1;
                    }
                }
                _lexemes.Add(new Lexeme(LexemeType.Сравнение, val, _current));
                _current.Token = @"";
            }

			if (_current.Symbol == '=')
            {
                if (_text[_currentProgramSymbolIndex + 1] == '=')
                {
                    _lexemes.Add(new Lexeme(_current));
                    _current.Token = @"";
                    _lexemes.Add(new Lexeme(LexemeType.Сравнение, "==", _current));
                    _programIndexToSkip = _currentProgramSymbolIndex + 2;
                }
                else
                {
                    if (_text[_currentProgramSymbolIndex - 1] != '=' &&
                	    _text[_currentProgramSymbolIndex - 1] != '<' &&
                	    _text[_currentProgramSymbolIndex - 1] != '>')
                        throw new ScanerException(ScanerExceptionReason.InvalidComparison, "ошибка оператора сравнения");
                }
            }
		}

		private void AddAssignmentTokens()
		{
            _lexemes.Add(new Lexeme(_current));
            _current.Token = @"";
            _lexemes.Add(new Lexeme(LexemeType.Присваивание, _current.Symbol.ToString(), _current));
		}

		private void AddBinaryOrUnarTokens()
		{
            int j0 = _currentProgramSymbolIndex - 1;
            for (; j0 >= 0; j0--)
            {
                if (UnimportantDelimeters.Contains(_text[j0])) continue;
                break;
            }

            if (_text[j0] == ':' || _text[j0] == '(' || _text[j0] == ',')
            {
                _lexemes.Add(new Lexeme(LexemeType.КонстантаВЕЩ, "0", _current));
                _lexemes.Add(new Lexeme(LexemeType.Разделитель, _current.Symbol.ToString(), _current));
                _current.Token = @"";
            }
            else
            {
                if (_current.Token.Length > 0)
                {
                    _lexemes.Add(new Lexeme(_current));
                }
                _lexemes.Add(new Lexeme(LexemeType.Разделитель, _current.Symbol.ToString(), _current));
                _current.Token = @"";
            }
		}

		private void AddConstantTokens()
		{
			// константы real
            if (_current.Symbol == '.')
            {
            	if (_current.Token.Length == 0 &&
            	    !Char.IsDigit(_text[_currentProgramSymbolIndex + 1]))
            	{
            		_lexemes.Add(new Lexeme(LexemeType.КонецВыражения, _current.Symbol.ToString(), _current.Line, _current.SymbolInLine - 1));
            		return;
            	}

                if (_current.Token.Length > 0 &&
            	    !Char.IsDigit(_current.Token[0]) && _current.Token[0] != '-')
                {
                    _lexemes.Add(new Lexeme(_current));
                    _lexemes.Add(new Lexeme(LexemeType.Разделитель, _current.Symbol.ToString(), _current));
                    _current.Token = @"";
                    return;
                }
            }

            // константа string
            if (_current.Symbol == '"')
            {
                int j0 = _text.IndexOf('"', _currentProgramSymbolIndex + 1);
                if (j0 == -1)
                	throw new ScanerException(ScanerExceptionReason.InfiniteStringLiteral, "незакрытая строковая константа");
                _lexemes.Add(new Lexeme(LexemeType.КонстантаСТР, _text.Substring(_currentProgramSymbolIndex, j0 - _currentProgramSymbolIndex + 1), _current));
                _currentProgramSymbolIndex = j0;
                _current.IsStringLiteral = !_current.IsStringLiteral;
                _current.Token = @"";
                return;
            }

            _current.Token += _current.Symbol;
		}

		private void RemoveEmptyTokensCascade()
		{
			for (int i = 0; i < _lexemes.Count; i++)
            {
				if (_lexemes[i].Value == "")
                {
                    for (int j0 = _lexemes.Count - 1; j0 >= i + 1; j0--)
                    {
                    	_lexemes[j0].Identifier = _lexemes[j0 - 1].Identifier;
                    }
                    _lexemes.RemoveAt(i);
                    i--;
                }
            }
		}

		private void ReInitTokensIdentifiers()
		{
			int min = _lexemes.Min(token => token.Identifier) - 1;
			for(int i = 0; i < _lexemes.Count; i++)
			{
				_lexemes[i].Identifier -= min;
			}
		}

		#endregion

		#region Identifiers resolving support methods

		private DataTypes GetTypeForLexeme(string tokenValue)
		{
            try
            {
            	DataTypes dt;
	        	if (!DataTypesExtensions.TryParse(tokenValue, out dt))
	                throw new IdentifierException(IdentifierExceptionReason.IdentifierTypeNotSpecified, "Не указан тип данных для " + tokenValue);
	        	return dt;
            }
            catch (Exception e)
            {
                throw new IdentifierException(
            		IdentifierExceptionReason.IdentifierTypeNotSpecified,
            		"Не указан тип данных для " + tokenValue ?? "undefined", e);
            }
		}

		/// <summary>
		/// Checks identifiers registered in the table already
		/// The method searches by identifiers name/value
		/// </summary>
		/// <param name="tokenValue">Value to search</param>
		/// <returns>The identifier or <c>null</c> if no one registered yet</returns>
		private Identifier GetIdentifier(string tokenValue)
		{
			return this.Identifiers.FirstOrDefault(
				(ident) => ident.Name == tokenValue || ident.Value == tokenValue);
		}

		/// <summary>
		/// Builds constant identifier value
		/// </summary>
		/// <param name="currentIndex">Index of the token-identifier name</param>
		/// <returns>Constant value</returns>
		private string GetConstantValue(int currentIndex)
        {
            string s = @"";
            int F = 0;
            for (int j = currentIndex + 2; j < _lexemes.Count; j++)
            {
                if (_lexemes[j].Value == "(") F++;
                if (_lexemes[j].Value == ";" || _lexemes[j].Value == "," ||
                    (_lexemes[j].Value == ")" && F == 0)) break;
                else s += _lexemes[j].Value;
            }
            return s;
        }

		/// <summary>
		/// Creates new iterator as separate unique variable
		/// </summary>
		/// <param name="iteratorName">Name of new iteration variable</param>
		/// <returns>New iteration variable generated by the parser</returns>
		public Identifier CreateIterator(string iteratorName)
		{
			Identifier id = new Identifier(iteratorName, DataTypes.ВЕЩ, "0", false);
            _identifiers.Add(id);
            return id;
		}

		#endregion

		#region Components interaction

		public Lexeme GetToken(int tokenIndex)
		{
			if (tokenIndex < 0)
				throw new ParserException(ParserExceptionReason.InvalidLexemeIndex);
			if (tokenIndex >= Lexemes.Count())
                throw new ParserException(ParserExceptionReason.EndOfProgram, "EOP");
            return Lexemes.ElementAt(tokenIndex);
		}

		public int GetFirstOccurenceOfIdentifier(Lexeme lex)
		{
			for (int i = 0; i < Lexemes.Count(); i++)
			{
				if (Lexemes.ElementAt(i).IdentifierReference == lex.IdentifierReference)
					return lex.IdentifierReference - 1;
			}
			return -1;
		}

		#endregion
	}
}
