﻿using System;

namespace SSqlTranslator.Language.Scaner
{
	public enum KeyWords
	{
		В,
		ВЕЩ,
		ВСТАВИТЬ,
		ВЫБРАТЬ,
		ВЫВОД,
		ГДЕ,
		ДЛЯ,
		ЕСЛИ,
		И,
		ИЗ,
		ИЛИ,
		ИНАЧЕ,
		КЛЮЧ,
		ЛОГ,
		МОДИФ,
		НАИБ,
		НАИМ,
		НЕ,
		СВЯЗЬ,
		СОЗДАТЬ,
		СРЕДНЕЕ,
		СТР,
		СЧЕТЧИК,
		ТАБЛИЦУ,
		ТО,
		УДАЛИТЬ
	}
}
