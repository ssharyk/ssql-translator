﻿using System;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Represents current state of scanning process
	/// </summary>
	public struct ScanerState
	{
		public int Line { get; set; }
		public int SymbolInLine { get; set; }
		public char Symbol { get; set; }
		public string Token { get; set; }

		public bool IsStringLiteral { get; set; }
	}
}
