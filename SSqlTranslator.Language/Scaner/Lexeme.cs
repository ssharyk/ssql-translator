﻿using System;
using System.Linq;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Represents single token of the script
	/// </summary>
	public class Lexeme
	{
		#region Properties

		/// <summary>
		/// Gets ID of the token
		/// </summary>
		public int Identifier { get; set; }

		/// <summary>
		/// Gets string value of the token
		/// </summary>
		public string Value { get; private set; }

		/// <summary>
		/// Gets type of the token
		/// </summary>
		public LexemeType Type { get; set; }

		/// <summary>
		/// Gets number of line in source code where the token begins
		/// </summary>
		public int Line { get; private set; }

		/// <summary>
		/// Gets number of symbol in source code line where the token begins
		/// </summary>
		public int Symbol { get; private set; }

		/// <summary>
		/// Gets number of identifier (var / const)
		/// </summary>
		public int IdentifierReference { get; set; }

		#endregion

		#region Construction

		private static int Id = 0;
		private static int GetUniqueId()
		{
			return ++Id;
		}

		public Lexeme(ScanerState state)
			: this(LexemeType.__, state.Token, state)
		{
		}

		public Lexeme(LexemeType type, string value, ScanerState state)
			: this(type, value, state.Line, state.SymbolInLine-1 - (value ?? "").Length)
		{
		}

		public Lexeme(LexemeType type, string value, int line, int symb)
		{
			this.Identifier = GetUniqueId();
			this.Type = type;
			this.Value = (value ?? "").Trim().ToUpper();
			this.Line = line;
			this.Symbol = symb;
		}

		#endregion

		#region Equals and GetHashCode implementation

		public override bool Equals(object obj)
		{
			Lexeme other = obj as Lexeme;
			if (other == null)
				return false;
			return
				this.Type == other.Type &&
				this.Value == other.Value &&
				this.IdentifierReference == other.IdentifierReference &&
				this.Line == other.Line &&
				this.Symbol == other.Symbol;
		}

		public override int GetHashCode()
		{
			return Identifier;
		}

		public static bool operator ==(Lexeme lhs, Lexeme rhs)
		{
			if (ReferenceEquals(lhs, rhs))
				return true;
			if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
				return false;
			return lhs.Equals(rhs);
		}

		public static bool operator !=(Lexeme lhs, Lexeme rhs)
		{
			return !(lhs == rhs);
		}

		#endregion

		public override string ToString()
		{
			return string.Format("[Lexeme ID={0}, Value='{1}', Type={2}, Line={3}, Symbol={4}, IDReference={5}]", 
			                     Identifier, Value, Type, Line, Symbol, IdentifierReference);
		}

		#region Support methods

		public bool IsIdentifier =>
			Type == LexemeType.ИмяКонстанты ||
			Type == LexemeType.ИмяПеременной ||
			Type.ToString().StartsWith("Константа");

		public bool IsDataType =>
			Enum.GetNames(typeof(DataTypes)).Contains(Value);

		#endregion
	}
}
