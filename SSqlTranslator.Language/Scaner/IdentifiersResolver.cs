﻿using System;
using System.Collections.Generic;
using System.Linq;
using SSqlTranslator.Language.Infrastructure;

namespace SSqlTranslator.Language.Scaner
{
	public class IdentifiersResolver : IIdentifiersResolver
	{
		#region

		private readonly ITokenizer _scaner;

		/// <summary>
		/// Gets collection of used identifiers (var / const)
		/// </summary>
		public IList<Identifier> Identifiers { get; private set; }

		#endregion

		#region Construction

		public IdentifiersResolver(ITokenizer tokenizer)
		{
			this._scaner = tokenizer;
			this.Identifiers = new List<Identifier>();
		}

		#endregion

		#region Main methods

		public int BuildIdentifiersTable()
		{
			for (int i = 0; i < _scaner.Lexemes.Count; i++)
            {
				Lexeme token = _scaner.Lexemes[i];
				DefineIdentifier(i);
				NotifyUnresolvedSymbol(i);
			}
			return Identifiers.Count;
		}

		private void DefineIdentifier(int currentIndex)
		{
			Lexeme token = _scaner.Lexemes[currentIndex];
            if (token.Type.ToString().StartsWith("Константа"))
            {
                String l = token?.Type.ToString().Substring("Константа".Length, 3);
                DataTypes dt = GetTypeForLexeme(l);
                Identifier registeredIdentifier = GetIdentifier(token.Value);
                if (registeredIdentifier == null)
                {
                	Identifier newIdentifier = new Identifier(null, dt, token.Value, true);
                    Identifiers.Add(newIdentifier);
                    token.IdentifierReference = newIdentifier.Number;
                }
                else
                {
                    token.IdentifierReference = registeredIdentifier.Number;
                }
            }

            if (token.Type == LexemeType.ИмяКонстанты)
            {
            	if (currentIndex == 0)
                {
                	throw new IdentifierException(
                		IdentifierExceptionReason.IdentifierTypeNotSpecified,
                		"Не указан тип данных для " + _scaner.Lexemes[0]?.Value ?? "undefined");
                }
            	DataTypes dt = GetTypeForLexeme(_scaner?.Lexemes?[currentIndex - 1]?.Value);
                Identifier registeredIdentifier = GetIdentifier(token.Value);
                if (registeredIdentifier == null)
                {
                	Identifier newIdentifier = new Identifier(token.Value, dt, GetConstantValue(currentIndex), true);
                    Identifiers.Add(newIdentifier);
                    token.IdentifierReference = newIdentifier.Number;
                }
                else
                {
                	if (_scaner.Lexemes[currentIndex + 1].Value == ":")
                        throw new IdentifierException(IdentifierExceptionReason.ConstantOverriding, "Константе не может быть присвоено новое значение");

                    token.IdentifierReference = registeredIdentifier.Number;
                }
            }

            if (token.Type == LexemeType.ИмяПеременной)
            {
                Identifier registeredIdentifier = GetIdentifier(token.Value);
                if (registeredIdentifier == null)
                {
                    string val = @"";
                    if (_scaner.Lexemes[currentIndex + 1].Value != ":") val = "";
                    else val = GetConstantValue(currentIndex);

                    if (currentIndex == 0)
                    {
                    	throw new IdentifierException(
                    		IdentifierExceptionReason.IdentifierTypeNotSpecified,
                    		"Не указан тип данных для " + _scaner.Lexemes[0]?.Value ?? "undefined");
                    }
                    DataTypes dt = GetTypeForLexeme(_scaner?.Lexemes?[currentIndex - 1]?.Value);
                    Identifier newIdentifier = new Identifier(token.Value, dt, val, false);
                    Identifiers.Add(newIdentifier);
                    token.IdentifierReference = newIdentifier.Number;
                }
                else
                {
                	if (_scaner.Lexemes[currentIndex + 1].Value == ":")
                		Identifiers[registeredIdentifier.Number].Value = GetConstantValue(currentIndex);

                    token.IdentifierReference = registeredIdentifier.Number;
                }
            }
		}

		private void NotifyUnresolvedSymbol(int currentIndex)
		{
			Lexeme token = _scaner.Lexemes[currentIndex];
			if (currentIndex > 0 &&
			    (token.Type.ToString().StartsWith("Имя") || token.Type.ToString().StartsWith("Константа")) &&
			    (_scaner.Lexemes[currentIndex - 1].Type.ToString().StartsWith("Имя") ||
			     _scaner.Lexemes[currentIndex - 1].Type.ToString().StartsWith("Константа")))
                    throw new IdentifierException(IdentifierExceptionReason.DelimerterRequired, "Требуется разделитель после '" + _scaner.Lexemes[currentIndex-1].Value + "' в строке " + token.Line);


                if (token.Type == LexemeType.ИмяКонстанты || token.Type == LexemeType.ИмяПеременной ||
                    token.Type == LexemeType.ИмяПоля || token.Type == LexemeType.ИмяТаблицы)
                {
                    foreach (char c in token.Value)
                    {
                        if (c != '_' && !Char.IsLetterOrDigit(c))
                        	throw new IdentifierException(IdentifierExceptionReason.UnknownSymbol, "странный символ [" + c + "] в " + token.Value + "\nСтрока " + token.Line + " символ " + token.Symbol);
                    }
                }

                if (token.Type == LexemeType.__)
                	throw new IdentifierException(IdentifierExceptionReason.UnknownSymbol, "странный символ в " + token.Value);
		}

		#endregion

		#region Support methods

		private DataTypes GetTypeForLexeme(string tokenValue)
		{
            try
            {
            	DataTypes dt;
	        	if (!DataTypesExtensions.TryParse(tokenValue, out dt))
	                throw new IdentifierException(IdentifierExceptionReason.IdentifierTypeNotSpecified, "Не указан тип данных для " + tokenValue);
	        	return dt;
            }
            catch (Exception e)
            {
                throw new IdentifierException(
            		IdentifierExceptionReason.IdentifierTypeNotSpecified,
            		"Не указан тип данных для " + tokenValue ?? "undefined", e);
            }
		}

		/// <summary>
		/// Checks identifiers registered in the table already
		/// The method searches by identifiers name/value
		/// </summary>
		/// <param name="tokenValue">Value to search</param>
		/// <returns>The identifier or <c>null</c> if no one registered yet</returns>
		private Identifier GetIdentifier(string tokenValue)
		{
			return this.Identifiers.FirstOrDefault(
				(ident) => ident.Name == tokenValue || ident.Value == tokenValue);
		}

		/// <summary>
		/// Builds constant identifier value
		/// </summary>
		/// <param name="currentIndex">Index of the token-identifier name</param>
		/// <returns>Constant value</returns>
		private string GetConstantValue(int currentIndex)
        {
            string s = @"";
            int F = 0;
            for (int j = currentIndex + 2; j < _scaner.Lexemes.Count; j++)
            {
                if (_scaner.Lexemes[j].Value == "(") F++;
                if (_scaner.Lexemes[j].Value == ";" || _scaner.Lexemes[j].Value == "," ||
                    (_scaner.Lexemes[j].Value == ")" && F == 0)) break;
                else s += _scaner.Lexemes[j].Value;
            }
            return s;
        }

		#endregion
	}
}
