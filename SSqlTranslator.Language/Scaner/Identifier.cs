﻿using System;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Represents data unit that can keep value (var / const)
	/// </summary>
	public class Identifier
	{
		#region Properties

		public int Number { get; private set; }
        public string Name { get; private set; }
        public DataTypes Type { get; private set; }
        public string Value { get; set; }
        public bool IsConst { get; private set; }

        #endregion

        #region Construction and update

        private static int Id = 0;
		private static int GetUniqueNumber()
		{
			return ++Id;
		}
		public static void Reset()
		{
			Id = 0;
		}

        public Identifier(string name, DataTypes type, string value, bool isConst)
        {
        	Number = GetUniqueNumber();
        	Name = name;
        	Type = type;
        	Value = value;
        	IsConst = isConst;
        }

        #endregion

        #region Equality

        /// <summary>
        /// Checks if two <see cref="SSqlTranslator.Language.Scaner.Identifier"/>s reference to single script data unit
		/// The method does not compare current values if IDs. Use <c>==</c> operator for such checking         
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><c>true</c> if the specified object is equal to the current object; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			Identifier other = obj as Identifier;
			if (other == null)
				return false;
			return
				this.Name == other.Name &&
				this.Type == other.Type &&
				this.IsConst == other.IsConst;
		}

		public override int GetHashCode()
		{
			return Number;
		}

		public static bool operator ==(Identifier lhs, Identifier rhs)
		{
			if (ReferenceEquals(lhs, rhs))
				return true;
			if (ReferenceEquals(lhs, null) ||
			    ReferenceEquals(rhs, null))
				return false;
			return lhs.Equals(rhs) && lhs.Value == rhs.Value;
		}

		public static bool operator !=(Identifier lhs, Identifier rhs)
		{
			return !(lhs == rhs);
		}

        #endregion

		public override string ToString()
		{
			return string.Format("[Identifier Number={0}, Name={1}, Type={2}, Value={3}, IsConst={4}]", 
			                     Number, Name, Type, Value, IsConst);
		}
	}
}
