﻿using System;
using System.Collections.Generic;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Scans prepared tokens to construct identifiers table
	/// </summary>
	public interface IIdentifiersResolver
	{
		/// <summary>
		/// Constructs identifiers table
		/// </summary>
		/// <returns>Number of identifiers found</returns>
		int BuildIdentifiersTable();

		/// <summary>
		/// Gets collection of used identifiers (var / const)
		/// </summary>
		IList<Identifier> Identifiers { get; }
	}
}
