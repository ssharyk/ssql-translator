﻿using System;

namespace SSqlTranslator.Language.Scaner
{
	/// <summary>
	/// Enumerates possible types of tokens
	/// </summary>
	public enum LexemeType
    {
        КлючевоеСлово,
        ИмяПеременной,
        ИмяТаблицы,
        ИмяПоля,
        ИмяКонстанты,
        КонстантаВЕЩ,
        КонстантаЛОГ,
        КонстантаСТР,
        Разделитель,
        Присваивание,
        Сравнение,
        КонецВыражения,
        Унарный,
        __
    }
}
