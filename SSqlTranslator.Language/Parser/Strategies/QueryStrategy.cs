﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses INSERT/MODIFY/DELETE/SELECT queries into RPN
	/// </summary>
	public class QueryStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public QueryStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			Lexeme tableName = _scaner.GetToken(state.NextToken());
			Lexeme command = _scaner.GetToken(state.NextToken());
			KeyWords commandKw;
			try
			{
				commandKw = (KeyWords) Enum.Parse(typeof(KeyWords), command.Value);
			}
			catch(ArgumentException)
			{
				ParserException.HandleRequirement(command, "имя команды");
				yield break;
			}

			IEnumerable<Command> result = null;
			switch (commandKw)
			{
				case KeyWords.ВСТАВИТЬ:
					result = (new QueryInsertStrategy(_scaner)).Parse(state);
					break;
				case KeyWords.МОДИФ:
					result = (new QueryModifyStrategy(_scaner)).Parse(state);
					break;
				case KeyWords.УДАЛИТЬ:
					result = (new QueryDeleteStrategy(_scaner)).Parse(state);
					break;
				case KeyWords.ВЫБРАТЬ:
					result = (new QuerySelectStrategy(_scaner)).Parse(state);
					break;
			}
			if (result == null)
			{
				ParserException.HandleRequirement(command, "unrecognized command");
				yield break;
			}
			
			if (commandKw != KeyWords.ВЫБРАТЬ)
			{
				yield return tableName.Value;
				state.Count++;
			}
			foreach (Command c in result)
			{
				yield return c;
			}
			if (commandKw != KeyWords.ВЫБРАТЬ)
			{
				yield return command.Value;
			}
		}
	}
}
