﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Parser;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses single operand to keep it in stack but not register in RPN
	/// </summary>
	internal class StackStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public StackStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			state.OperatorsStack.Push(_scaner.GetToken(state.CurrentTokenIndex).Value);
			yield break;
		}
	}
}
