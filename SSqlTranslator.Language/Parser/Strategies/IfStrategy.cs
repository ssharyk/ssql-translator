﻿using System;
using System.Collections.Generic;
using System.Linq;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses IF construction into RPN
	/// </summary>
	public class IfStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public IfStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			int existing = state.Count;
			Lexeme tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
            	ParserException.HandleRequirement(tok, "(");
                yield break;
            }
            state.OperatorsStack.Push("(");

            IParsingStrategy expressionStrategy = new ExpressionStrategy(_scaner);
            List<Command> rpn = expressionStrategy.Parse(state).ToList();
            int currentLabel = state.NextLabel();
            rpn.Add(new DirectiveLabelCommand(currentLabel));
            rpn.Add("УПЛ");
            state.Count += 2;

            tok = _scaner.GetToken(state.CurrentTokenIndex);
            if (tok.Value != KeyWords.ТО.ToString())
            {
            	ParserException.HandleRequirement(tok, KeyWords.ТО.ToString());
                yield break;
            }
            int thenStart = rpn.Count;
            do
            {
            	tok = _scaner.GetToken(state.CurrentTokenIndex);
                IParsingStrategy nestedParser = (new ParsersFactory(_scaner)).Create(tok);
	        	if (nestedParser != null)
	        	{
	        		rpn.AddRange(nestedParser.Parse(state));
	        	}
	        	tok = _scaner.GetToken(state.CurrentTokenIndex);
	        	if (tok.Value == "." || tok.Value == KeyWords.ИНАЧЕ.ToString())
                {
                	break;
                }
                state.NextToken();
            } while (true);
            int thenFinish = rpn.Count;
            state.Labels[state.CurrentLabel] = existing + rpn.Count - (thenFinish - thenStart) - 1;     // метка по УПЛ

            tok = _scaner.GetToken(state.CurrentTokenIndex);
            if (_scaner.GetToken(state.PeekNextToken()).Value == KeyWords.ИНАЧЕ.ToString())
            {
            	tok = _scaner.GetToken(state.NextToken());
            }
            if (tok.Value == KeyWords.ИНАЧЕ.ToString())
            {
            	rpn.Add(new DirectiveLabelCommand(state.NextLabel()));
                rpn.Add("БП");
                state.Count += 2;                
                state.Labels[currentLabel] = existing + rpn.Count;

                int lblElse = state.CurrentLabel;
                do
                {
                	tok = _scaner.GetToken(state.NextToken());
                	IParsingStrategy nestedParser = (new ParsersFactory(_scaner)).Create(tok);
		        	if (nestedParser != null)
		        	{
		        		rpn.AddRange(nestedParser.Parse(state));
		        	}
		        	tok = _scaner.GetToken(state.CurrentTokenIndex);
                } while (tok.Value != ".");

                state.Labels[lblElse] = rpn.Count + existing;
            }

            if (tok.Value != ".")
            {
            	ParserException.HandleRequirement(tok, ".");
                yield break;
            }

            foreach(Command command in rpn) yield return command;
		}
	}
}
