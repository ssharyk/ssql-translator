﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Parser.Commands;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Contains logic for parsing single expression
	/// </summary>
	public interface IParsingStrategy
	{
		/// <summary>
		/// Executes parsing of specific program statement
		/// </summary>
		/// <param name="state">Current state of global parsing process</param>
		/// <returns>Collection of commands to execute</returns>
		IEnumerable<Command> Parse(ParsingState state);
	}
}
