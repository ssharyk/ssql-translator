﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Parser;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses single operand to convert it to RPN item
	/// </summary>
	internal class OperandStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public OperandStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			yield return _scaner.GetToken(state.CurrentTokenIndex).Value;
		}
	}
}
