﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses CREATE 'Table/key/link' into RPN
	/// </summary>
	public class CreateStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public CreateStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			Lexeme objectToCreate = _scaner.GetToken(state.NextToken());
			IEnumerable<Command> result = null;
			if (objectToCreate.Value == KeyWords.ТАБЛИЦУ.ToString())
			{
				result = (new CreateTableStrategy(_scaner)).Parse(state);
			}
			else if (objectToCreate.Value == KeyWords.КЛЮЧ.ToString())
			{
				result = (new CreateKeyStrategy(_scaner)).Parse(state);
			}
			else if (objectToCreate.Value == KeyWords.СВЯЗЬ.ToString())
			{
				result = (new CreateLinkStrategy(_scaner)).Parse(state);
			}
			else
			{
				ParserException.HandleRequirement(objectToCreate, "таблица/ключ/связь");
				yield break;
			}
			foreach(Command command in result) yield return command;
		}
	}
}
