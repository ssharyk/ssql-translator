﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses CREATE 'Key' into RPN
	/// </summary>
	public class CreateKeyStrategy
	{
		private readonly ITokenizer _scaner;

		public CreateKeyStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			Lexeme tableName = _scaner.GetToken(state.NextToken());
            if (tableName.Type != LexemeType.ИмяТаблицы)
            {
                ParserException.HandleRequirement(tableName, "имя таблицы");
                yield break;
            }
            string tblName = tableName.Value;

            Lexeme delim = _scaner.GetToken(state.NextToken());
            if (delim.Value != "(")
            {
                ParserException.HandleRequirement(delim, "(");
                yield break;
            }

            int parameters = 0;
            Lexeme fieldName;
            do
            {
                fieldName = _scaner.GetToken(state.NextToken());
                if (fieldName.Type != LexemeType.ИмяПоля)
                {
                    ParserException.HandleRequirement(fieldName, "имя поля");
                    yield break;
                }
                parameters++;
                yield return fieldName.Value;

                delim = _scaner.GetToken(state.NextToken());
            } while (delim.Value == ",");

            delim = _scaner.GetToken(state.CurrentTokenIndex);
            if (delim.Value != ")")
            {
                ParserException.HandleRequirement(delim, ")");
                yield break;
            }

            delim = _scaner.GetToken(state.NextToken());
            if (delim.Value != ".")
            {
                ParserException.HandleRequirement(delim, ".");
                yield break;
            }

            yield return parameters.ToString();
            yield return tblName;
            yield return KeyWords.СОЗДАТЬ + "_К";
		}
	}
}
