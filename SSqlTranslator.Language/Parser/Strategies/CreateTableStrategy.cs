﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses CREATE 'Table' into RPN
	/// </summary>
	public class CreateTableStrategy
	{
		private readonly ITokenizer _scaner;

		public CreateTableStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			// имя
			Lexeme tableName = _scaner.GetToken(state.NextToken());
            if (tableName.Type != LexemeType.ИмяТаблицы)
            {
                ParserException.HandleRequirement(tableName, "имя таблицы");
                yield break;
            }
            yield return tableName.Value;

            Lexeme tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
                ParserException.HandleRequirement(tok, "(");
                yield break;
            }

            int parameters = 1;
            Lexeme dataType, fieldName, delim;
            do
            {
                dataType = _scaner.GetToken(state.NextToken());
                fieldName = _scaner.GetToken(state.NextToken());
                delim = _scaner.GetToken(state.NextToken());

                if (!dataType.IsDataType)
                {
                    ParserException.HandleRequirement(tok, "Тип данных");
                    yield break;
                }

                if (fieldName.Type != LexemeType.ИмяПоля)
                {
                    ParserException.HandleRequirement(fieldName, "Имя поля таблицы");
                    yield break;
                }

                if (delim.Value != "," && delim.Value != ")")
                {
                    ParserException.HandleRequirement(delim, ", или )");
                    yield break;
                }

                yield return dataType.Value;
                yield return fieldName.Value;
                parameters += 2;
                state.Count += 2;
            } while (delim.Value != ")");

            tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != ".")
            {
                ParserException.HandleRequirement(tok, ".");
                yield break;
            }

            yield return parameters.ToString();
            yield return KeyWords.СОЗДАТЬ + "_Т";
            state.Count += 2;
		}
	}
}
