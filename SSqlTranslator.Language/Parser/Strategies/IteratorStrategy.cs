﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Explicit creates RPN items for iterators
	/// </summary>
	public class IteratorStrategy
	{
		private readonly ITokenizer _scaner;
		private readonly string _tableName;
		private IterationMode _iterationMode;
		
		private string IteratorName =>
			_tableName + "_ITERATOR_" + ((int)_iterationMode);
		
		public int Label1_BreakLoop { get; private set; }
		public int Label3_NewIteration { get; private set; }
		public int Label2Hop { get; private set; }
		public int Label3Hop { get; private set; }

		public IteratorStrategy(ITokenizer scaner, string tableName, IterationMode mode)
		{
			this._scaner = scaner;
			this._tableName = tableName;
			this._iterationMode = mode;
		}

		public IEnumerable<Command> ParseStart(ParsingState state)
		{
			yield return "ВЕЩ";
            _scaner.CreateIterator(IteratorName);
            yield return IteratorName;
            yield return "0";
            yield return ":";
            state.Count += 4;
            
            Label3Hop = state.Count;
            yield return IteratorName;
            yield return "$LENGTH";
            yield return "<";
            state.Count += 3;
            
            Label1_BreakLoop = state.NextLabel();
            yield return new DirectiveLabelCommand(Label1_BreakLoop);
            yield return "УПЛ";
            state.Count += 2;
		}
	
		public IEnumerable<Command> ParseEnd(ParsingState state)
		{
			Label2Hop = state.Count;
            yield return IteratorName;
            yield return IteratorName;
            yield return "1";
            yield return "+";
            yield return ":";
            state.Count += 5;

            Label3_NewIteration = state.NextLabel();
            yield return new DirectiveLabelCommand(Label3_NewIteration);
            yield return "БП";
            state.Count += 2;
		}
	}

	public enum IterationMode
	{
		Select = 1,
		Modify = 2,
		Delete = 3
	}
}
