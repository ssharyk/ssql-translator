﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses CREATE 'Key' into RPN
	/// </summary>
	public class CreateLinkStrategy
	{
		private readonly ITokenizer _scaner;

		public CreateLinkStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			Lexeme tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
                ParserException.HandleRequirement(tok, "(");
                yield break;
            }

            Tuple<Lexeme, Lexeme> qualifier = ParseOneLink(state);
            if (qualifier != null)
            {
            	yield return qualifier.Item1.Value;
            	yield return qualifier.Item2.Value;
            	state.Count += 2;
            }
            tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != ",")
            {
                ParserException.HandleRequirement(tok, ",");
                yield break;
            }
            qualifier = ParseOneLink(state);
            if (qualifier != null)
            {
            	yield return qualifier.Item1.Value;
            	yield return qualifier.Item2.Value;
            	state.Count += 2;
            }

            state.NextToken();
            yield return KeyWords.СОЗДАТЬ + "_С";
            state.Count++;
		}

		/// <summary>
		/// Parses single line on format (TableName!KeyFieldName)
		/// </summary>
		/// <param name="state">Current parsing state</param>
		/// <returns>Tuple of lexemes of table and field names</returns>
		private Tuple<Lexeme, Lexeme> ParseOneLink(ParsingState state)
        {
            Lexeme tableName = _scaner.GetToken(state.NextToken());
            if (tableName.Type != LexemeType.ИмяТаблицы)
            {
                ParserException.HandleRequirement(tableName, "Имя таблицы");
                return null;
            }

            Lexeme delim = _scaner.GetToken(state.NextToken());
            if (delim.Value!="!")
            {
                ParserException.HandleRequirement(delim, "!");
                return null;
            }

            Lexeme fieldName = _scaner.GetToken(state.NextToken());
            if (fieldName.Type != LexemeType.ИмяПоля)
            {
                ParserException.HandleRequirement(fieldName, "Имя поля");
                return null;
            }
            return Tuple.Create(tableName, fieldName);
        }
	}
}
