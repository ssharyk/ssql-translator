﻿using System;
using System.Collections.Generic;

using System.Linq;
using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses assignment statement to RPN
	/// </summary>
	public class AssignmentStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public AssignmentStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			Lexeme prev = _scaner.GetToken(state.CurrentTokenIndex - 1);
			Lexeme type = _scaner.GetToken(state.CurrentTokenIndex - 2);
			int identifierIndex = _scaner.GetFirstOccurenceOfIdentifier(prev);
			if (identifierIndex == -1)
			{
				if (!type.IsDataType)
				{
					ParserException.HandleRequirement(type, "тип данных");
					yield break;
				}
			}
			else
			{
				if (_scaner.Identifiers.ElementAt(identifierIndex).IsConst &&
				   prev.IdentifierReference - 1 != identifierIndex)
				{
					throw new ParserException(ParserExceptionReason.ConstantReAssignment);
				}
			}

            if (prev.Type == LexemeType.ИмяКонстанты ||
                prev.Type == LexemeType.ИмяПеременной ||
                prev.Type == LexemeType.ИмяПоля)
            {
                state.OperatorsStack.Push(":");
                IEnumerable<Command> rpn = new ExpressionStrategy(_scaner).Parse(state);
                foreach(Command c in rpn) yield return c;
            }
            else
            {
                throw new ParserException(ParserExceptionReason.InvalidAssignment, "Присваивание должно применяться к переменной/константе");
            }
		}
	}
}
