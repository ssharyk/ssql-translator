﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	public class ExpressionStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;
		private static readonly Dictionary<string, int> Priorities;
		private bool _isForEnd;

		#region Construction

		public ExpressionStrategy(ITokenizer scaner)
			: this(scaner, true)
		{
		}

		public ExpressionStrategy(ITokenizer scaner, bool isForEnd)
		{
			this._scaner = scaner;
			this._isForEnd = isForEnd;
		}

		static ExpressionStrategy()
		{
            Priorities = new Dictionary<string, int>();

            Priorities.Add("(", 0);
            Priorities.Add("[", 0);
            Priorities.Add("ЕСЛИ", 0);
            Priorities.Add("ДЛЯ", 0);

            Priorities.Add(")", 1);
            Priorities.Add("]", 1);
            Priorities.Add(",", 1);
            Priorities.Add(";", 1);
            Priorities.Add(".", 1);
            Priorities.Add("ТО", 1);
            Priorities.Add("ИНАЧЕ", 1);

            Priorities.Add(":", 2);

            Priorities.Add("ИЛИ", 3);

            Priorities.Add("И", 4);

            Priorities.Add("==", 5);
            Priorities.Add("<>", 5);

            Priorities.Add(">", 6);
            Priorities.Add(">=", 6);
            Priorities.Add("<", 6);
            Priorities.Add("<=", 6);

            Priorities.Add("+", 7);
            Priorities.Add("-", 7);

            Priorities.Add("*", 8);
            Priorities.Add("/", 8);
            Priorities.Add("%", 8);

            Priorities.Add("^", 9);
            Priorities.Add("~", 9);
            Priorities.Add("НЕ", 9);
		}

		#endregion

		public IEnumerable<Command> Parse(ParsingState state)
		{
			List<Command> commands = new List<Command>();
			string s="";
            do
            {
            	state.NextToken();
                Lexeme lex = _scaner.GetToken(state.CurrentTokenIndex);
                s = lex.Value;

                if (lex.Type.ToString().StartsWith("Имя") ||
                    lex.Type.ToString().StartsWith("Константа"))
                {
                	if (s?.Trim().Length > 0)
                	{
                		commands.Add(s);
                		state.Count++;
                	}
                }

                if (
                    (s[0] == '+' || s[0] == '-' || s[0] == '*' || s[0] == '/' || s[0]=='%' || s[0]=='^' ||
                    s[0] == '>' || s[0] == '<' || s[0] == '=' || s[0] == '~' ||
                    s == "ИЛИ" || s == "И" || s == "НЕ" ))
                {
                    while (state.OperatorsStack.Count > 0 &&
                	       Priorities[state.OperatorsStack.Peek()] >= Priorities[s])
                    {
                		string op = state.OperatorsStack.Pop();
                		if (op?.Trim().Length > 0)
                		{
                			commands.Add(op);
                			state.Count++;
                		}
                    }

                    if (state.OperatorsStack.Count == 0 ||
                	    Priorities[state.OperatorsStack.Peek()] < Priorities[s])
                    {
                        state.OperatorsStack.Push(s);
                    }
                }

                if (s[0] == '(')
                {
                    state.OperatorsStack.Push(s);
                }

                if (s[0] == ')')
                {
                    string s1 = "";
                    do
                    {
                        s1 = state.OperatorsStack.Pop();
                        if (s1?.Trim().Length > 0 && s1 != "(")
                        {
                        	commands.Add(s1);
                        	state.Count++;
                        }
                    } while (s1 != "(");
                }
            } while (s!=";" && s!="ТО" && s!="ИНАЧЕ" && s!="." && s!="," && s!="ГДЕ");

            if (_isForEnd)
            {
	            string s2="";
	            while (state.OperatorsStack.Count > 0)
	            {
	                s2 = state.OperatorsStack.Pop();
	                if (s2?.Trim().Length > 0 && s2 != "(" &&
	                    s2 != KeyWords.ВЕЩ.ToString() && s2 != KeyWords.ЛОГ.ToString() && s2 != KeyWords.СТР.ToString())
	                {
	                	commands.Add(s2);
	                	state.Count++;
	                }
	            }
            }

            return commands;
		}
	}
}
