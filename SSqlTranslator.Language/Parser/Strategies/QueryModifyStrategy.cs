﻿using System;
using System.Collections.Generic;

using System.Linq;
using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses MODIFY queries into RPN
	/// </summary>
	public class QueryModifyStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public QueryModifyStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			int parameters = 1;
            Lexeme tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
                ParserException.HandleRequirement(tok, "(");
                yield break;
            }
            state.OperatorsStack.Push("(");

            string tblName=_scaner.GetToken(state.CurrentTokenIndex - 2).Value;
            IteratorStrategy iterator = new IteratorStrategy(_scaner, tblName, IterationMode.Modify);
            List<Command> rpn = new List<Command>();
            rpn.AddRange(iterator.ParseStart(state));

            int start = rpn.Count;
            Lexeme nm;
            do
            {
                nm = _scaner.GetToken(state.NextToken());
                tok = _scaner.GetToken(state.NextToken());

                if (tok.Type != LexemeType.Присваивание)
                {
                    ParserException.HandleRequirement(tok, ":");
                    yield break;
                }

                parameters++;
                rpn.Add(nm.Value);
                state.Count++;
                IParsingStrategy expressionStrategy = new ExpressionStrategy(_scaner, false);
                rpn.AddRange(expressionStrategy.Parse(state));
                state.OperatorsStack.Push("(");
                rpn.Add(new AssignmentCommand());
                state.Count++;

                tok =_scaner.GetToken(state.CurrentTokenIndex);
            } while (tok.Type != LexemeType.КонецВыражения && tok.Value!="ГДЕ");

            int finish = rpn.Count;

            List<Command> body = rpn.GetRange(start, finish - start);
            rpn.RemoveRange(start, finish - start);

            tok =_scaner.GetToken(state.CurrentTokenIndex);
            bool isCond = false;
            IEnumerable<Command> condition = null;
            if (tok.Value == KeyWords.ГДЕ.ToString())
            {
                isCond = true;
                IParsingStrategy expressionStrategy = new ExpressionStrategy(_scaner, false);
                condition = expressionStrategy.Parse(state);
                rpn.AddRange(condition);
            }

            tok =_scaner.GetToken(state.CurrentTokenIndex);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                ParserException.HandleRequirement(tok, ".");
                yield break;
            }

            int lbl2 = state.NextLabel();
            if (isCond)
            {
            	rpn.Add(new DirectiveLabelCommand(state.CurrentLabel));
            	rpn.Add("УПЛ");
            	state.Count += 2;
            }

            rpn.AddRange(body);
            rpn.AddRange(iterator.ParseEnd(state));

            foreach(Command c in rpn) yield return c;

            /// TODO: research labels destination
            /// -1 for total count and -1 for table name
            state.Labels[iterator.Label1_BreakLoop] = state.Count - 1 - 1;
            state.Labels[lbl2] = iterator.Label2Hop - ((condition == null) ? 0 : condition.Count());
            state.Labels[iterator.Label3_NewIteration] = iterator.Label3Hop;
		}
	}
}