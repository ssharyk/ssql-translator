﻿using System;
using System.Collections.Generic;

using System.Linq;
using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses PRINT function
	/// </summary>
	public class PrintStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public PrintStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			int numberOfParameters = 0;
			Lexeme tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
                ParserException.HandleRequirement(tok, "(");
                yield break;
            }

            List<Command> rpn = new List<Command>();
            Lexeme val;
            do
            {
            	val = _scaner.GetToken(state.NextToken());
                if (val.Value == ")")
                {
                    break;
                }

                tok = _scaner.GetToken(state.PeekNextToken());
                if (tok.Value != "," && tok.Value != ")")
                {
                    ParserException.HandleRequirement(tok, ",");
                    foreach(Command c in rpn) yield return c;
                }
                if (val.Type != LexemeType.ИмяКонстанты &&
                    val.Type != LexemeType.ИмяПеременной &&
                    !val.Type.ToString().StartsWith("Константа"))
                {
                    ParserException.HandleRequirement(val, "переменная/константа");
                    foreach(Command c in rpn) yield return c;
                }

                numberOfParameters++;
                IParsingStrategy nestedParser = (new ParsersFactory(_scaner)).Create(val);
	        	if (nestedParser != null)
	        	{
	        		rpn.AddRange(nestedParser.Parse(state));
	        		state.NextToken();
	        	}
            } while (tok.Value != ")");

            val = _scaner.GetToken(state.NextToken());
            if (val.Value != ".")
            {
                ParserException.HandleRequirement(val, ".");
                foreach(Command c in rpn) yield return c;
            }

            rpn.Add(numberOfParameters.ToString());
            rpn.Add(KeyWords.ВЫВОД.ToString());
            state.Count += 2;
            foreach(Command c in rpn) yield return c;
		}
	}
}
