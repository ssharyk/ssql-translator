﻿using System;
using System.Collections.Generic;
using System.Linq;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses DELETE queries into RPN
	/// </summary>
	public class QuerySelectStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public QuerySelectStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			state.Back(2);
			List<Command> rpn = new List<Command>();
			Lexeme tok;
            bool isManyTables=false;
            string tblName="undefined";
            do
            {
                tok = _scaner.GetToken(state.NextToken());
                if (tok.Type != LexemeType.ИмяТаблицы)
                {
                    ParserException.HandleRequirement(tok, "имя таблицы");
                    yield break;
                }
                rpn.Add(tok.Value);
                state.Count++;

                if (tblName != "undefined")
                    isManyTables = true;

                tblName = tok.Value;
                tok = _scaner.GetToken(state.NextToken());
                if (tok.Value != "," && tok.Value != KeyWords.ВЫБРАТЬ.ToString())
                    ParserException.HandleRequirement(tok, ", или ВЫБРАТЬ");

            } while (tok.Value != KeyWords.ВЫБРАТЬ.ToString());

            if (tok.Value != KeyWords.ВЫБРАТЬ.ToString())
            {
                ParserException.HandleRequirement(tok, "команда (выбор)");
                yield break;
            }

            tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
                ParserException.HandleRequirement(tok, "(");
                yield break;
            }

            /// TODO: aggregation functions
            IteratorStrategy iterator = new IteratorStrategy(_scaner, tblName, IterationMode.Select);
            rpn.AddRange(iterator.ParseStart(state));
            int _aggrIndex = iterator.Label3Hop;
            int agInd = _aggrIndex;

            int bodyStart = rpn.Count;

            // выбираемые поля
            IEnumerable<Command> fields = ParseSelectedFields(state, tblName, isManyTables).ToList();
            rpn.AddRange(fields);

            int bodyFinish = rpn.Count;
            List<Command> body = rpn.GetRange(bodyStart, bodyFinish - bodyStart);
            bodyStart += (_aggrIndex - agInd);
            rpn.RemoveRange(bodyStart, bodyFinish - bodyStart);

            tok = _scaner.GetToken(state.NextToken());
            int lbl2 = state.NextLabel();
            List<Command> condition = new List<Command>();
            if (tok.Value == KeyWords.ГДЕ.ToString())
            {
                IParsingStrategy expressionStrategy = new ExpressionStrategy(_scaner, false);
                condition = expressionStrategy.Parse(state).ToList();
            }
            rpn.AddRange(condition);
            rpn.Add(new DirectiveLabelCommand(lbl2));
            rpn.Add("УПЛ");
            state.Count += 2;

            rpn.AddRange(body);

            IEnumerable<Command> iterEnd = iterator.ParseEnd(state);
            rpn.AddRange(iterEnd);

            state.Labels[iterator.Label1_BreakLoop] = state.Count + 1;
            state.Labels[lbl2] = iterator.Label2Hop;
            state.Labels[iterator.Label3_NewIteration] = iterator.Label3Hop + (_aggrIndex - agInd);

            int commandIndex = rpn.Count - 2;
            Command selectCommand = new Command(KeyWords.ВЫБРАТЬ.ToString());
            rpn.Insert(commandIndex, selectCommand);
            state.Count++;

            tok = _scaner.GetToken(state.CurrentTokenIndex);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                ParserException.HandleRequirement(tok, ".");
                yield break;
            }

            foreach(Command c in rpn) yield return c;
		}

		private IEnumerable<Command> ParseSelectedFields(ParsingState state, string tblName, bool isManyTables)
        {
            Lexeme nm, var, tok;
            List<Command> rpn = new List<Command>();

            do
            {
                // описание переменной
                var = _scaner.GetToken(state.NextToken());
                if (var.IsDataType)
                {
                    rpn.Add(var.Value);
                    state.Count++;
                    var = _scaner.GetToken(state.NextToken());
                }
                else
                {
                	ParserException.HandleRequirement(var, "тип переменной");
                	yield break;
                }

                if (var.Type != LexemeType.ИмяПеременной)
                {
                    ParserException.HandleRequirement(var, "переменная ");
                    yield break;
                }
                rpn.Add(var.Value);
                state.Count++;

                // присваивание
                tok = _scaner.GetToken(state.NextToken());
                if (tok.Type != LexemeType.Присваивание)
                {
                    ParserException.HandleRequirement(tok, ":");
                    yield break;
                }

                // имя поля
                nm = _scaner.GetToken(state.NextToken());
                if (nm.Value == "НАИБ" || nm.Value == "НАИМ" || nm.Value == "СРЕДНЕЕ")
                {
                    tok = _scaner.GetToken(state.NextToken());
                    if (tok.Value != "(")
                    {
                        ParserException.HandleRequirement(tok, "(");
                        yield break;
                    }

                    if (isManyTables)
                    {
                        Lexeme tb = _scaner.GetToken(state.NextToken());
                        tblName = tb.Value;
                        tok = _scaner.GetToken(state.NextToken());
                        if (tok.Value != "!")
                            ParserException.HandleRequirement(tok, "!");
                        //nm = _scaner.GetToken(state.NextToken());
                    }
                    rpn.Add(tblName);
                    state.Count++;

                    nm = _scaner.GetToken(state.NextToken());
                    if (nm.Type != LexemeType.ИмяПоля)
                    {
                        ParserException.HandleRequirement(nm, "имя поля");
                        yield break;
                    }

                    tok = _scaner.GetToken(state.NextToken());
                    if (tok.Value != ")")
                    {
                        ParserException.HandleRequirement(tok, ")");
                        yield break;
                    }

                    rpn.Add(nm.Value);
                    state.Count++;
                    Lexeme check = (isManyTables) ? _scaner.GetToken(state.CurrentTokenIndex - 5) : _scaner.GetToken(state.CurrentTokenIndex - 3);
                    rpn.Add(check.Value);
                    rpn.Add(":");
                    state.Count += 2;

                    tok = _scaner.GetToken(state.NextToken());
                    continue;
                }
                else
                {
                    if (isManyTables)
                    {
                        tblName = nm.Value;
                        tok = _scaner.GetToken(state.NextToken());
                        if (tok.Value != "!")
                            ParserException.HandleRequirement(tok, "!");
                        nm = _scaner.GetToken(state.NextToken());
                    }
                    rpn.Add(tblName);
                    state.Count++;

                    if (nm.Type != LexemeType.ИмяПоля)
                    {
                        ParserException.HandleRequirement(nm, "имя поля");
                        yield break;
                    }
                    rpn.Add(nm.Value);
                    state.Count++;
                }

                rpn.Add(":");
                state.Count++;

                tok = _scaner.GetToken(state.NextToken());
            } while (tok.Value != ")");

            foreach(Command c in rpn) yield return c;
        }
	}
}
