﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses INSERT queries into RPN
	/// </summary>
	public class QueryInsertStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public QueryInsertStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			int parameters = 1;
            Lexeme tok = _scaner.GetToken(state.NextToken());
            if (tok.Value != "(")
            {
                ParserException.HandleRequirement(tok, "(");
                yield break;
            }
            state.OperatorsStack.Push("(");

            Lexeme val;
            do
            {
                val = _scaner.GetToken(state.NextToken());
                yield return val.Value;
                IParsingStrategy expressionStrategy = new ExpressionStrategy(_scaner, false);
            	IEnumerable<Command> nestedRpn = expressionStrategy.Parse(state);
            	foreach(Command c in nestedRpn) yield return c;

                tok =_scaner.GetToken(state.CurrentTokenIndex);
                if (tok.Value != "," && tok.Value != ".")
                {
                    ParserException.HandleRequirement(tok, ", или )");
                    yield break;
                }

                parameters++;

                while (state.OperatorsStack.Count > 1)
                {
                	yield return state.OperatorsStack.Pop();
                }
            } while (tok.Value != ".");

            tok =_scaner.GetToken(state.CurrentTokenIndex);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                ParserException.HandleRequirement(tok, ".");
                yield break;
            }

            yield return parameters.ToString();
		}
	}
}
