﻿using System;
using System.Collections.Generic;
using System.Linq;

using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses FOREACH construction into RPN
	/// </summary>
	public class ForeachStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public ForeachStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
			int existing = state.Count;
			List<Command> rpn = new List<Command>();
			Lexeme tok = _scaner.GetToken(state.NextToken());

            int lbl1 = state.CurrentLabel;
            state.NextToken();
            state.NextToken();
            IParsingStrategy selectStrategy = new QuerySelectStrategy(_scaner);
            List<Command> selectQuery = selectStrategy.Parse(state).ToList();
            rpn.AddRange(selectQuery);
            int start = state.Labels[state.CurrentLabel - 1] - existing;
            List<Command> iteratorIncrement = rpn.GetRange(start, rpn.Count - start);
            rpn.RemoveRange(start, rpn.Count - start);

            // loop body
            List<Command> body = new List<Command>();
            do
            {
            	tok = _scaner.GetToken(state.NextToken());
                IParsingStrategy parser = (new ParsersFactory(_scaner)).Create(tok);
	        	if (parser != null)
	        	{
	        		body.AddRange(parser.Parse(state));
	        	}            	
            } while (tok.Type != LexemeType.КонецВыражения);
            rpn.AddRange(body);
            rpn.AddRange(iteratorIncrement);

            state.Labels[lbl1 + 1] = start + existing + body.Count()  + iteratorIncrement.Count; // body.Count; // existing + rpn.Count;// + selectQuery.Count - rest.Count - 8;//rpn.Count; //start + (body.Count- rest.Count);
            state.Labels[lbl1 + 2] = start + existing + body.Count() ; //state.Labels[lbl1 + 1] - iteratorIncrement.Count;

            return rpn;
		}
	}
}
