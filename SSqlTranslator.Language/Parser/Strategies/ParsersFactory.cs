﻿using System;

using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Instantiates new parser according with current token
	/// </summary>
	public class ParsersFactory
	{
		private readonly ITokenizer _scaner;

		public ParsersFactory(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IParsingStrategy Create(Lexeme token)
		{
			if (token.Value == ".")
				return null;
			if (token.IsDataType)
				return new StackStrategy(_scaner);
			if (token.IsIdentifier)
				return new OperandStrategy(_scaner);

			if (token.Type == LexemeType.Присваивание)
				return new AssignmentStrategy(_scaner);

			if (token.Type == LexemeType.КлючевоеСлово)
			{
				KeyWords keyword = (KeyWords) Enum.Parse(typeof(KeyWords), token.Value);
				switch(keyword)
				{
					case KeyWords.ЕСЛИ:
						return new IfStrategy(_scaner);
					case KeyWords.ВЫВОД:
						return new PrintStrategy(_scaner);
					case KeyWords.СОЗДАТЬ:
						return new CreateStrategy(_scaner);
					case KeyWords.В:
					case KeyWords.ИЗ:
						return new QueryStrategy(_scaner);
					case KeyWords.ДЛЯ:
						return new ForeachStrategy(_scaner);
					default:
						return null;
				}
			}
			throw new NotImplementedException();
		}
	}
}
