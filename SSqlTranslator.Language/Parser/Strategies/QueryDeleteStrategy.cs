﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser.Strategies
{
	/// <summary>
	/// Parses DELETE queries into RPN
	/// </summary>
	public class QueryDeleteStrategy : IParsingStrategy
	{
		private readonly ITokenizer _scaner;

		public QueryDeleteStrategy(ITokenizer scaner)
		{
			this._scaner = scaner;
		}

		public IEnumerable<Command> Parse(ParsingState state)
		{
            string tblName=_scaner.GetToken(state.CurrentTokenIndex - 1).Value;
            IteratorStrategy iterator = new IteratorStrategy(_scaner, tblName, IterationMode.Delete);
            List<Command> rpn = new List<Command>();
            rpn.AddRange(iterator.ParseStart(state));

            Lexeme tok = _scaner.GetToken(state.NextToken());
            int start = state.Count;

            if (tok.Value == KeyWords.ГДЕ.ToString())
            {
                IParsingStrategy expressionStrategy = new ExpressionStrategy(_scaner, false);
                rpn.AddRange(expressionStrategy.Parse(state));
            }

            int lbl2 = state.NextLabel();
            rpn.Add(new DirectiveLabelCommand(lbl2));
            rpn.Add("УПЛ");
            state.Count += 2;

            tok = _scaner.GetToken(state.CurrentTokenIndex);
            if (tok.Type != LexemeType.КонецВыражения)
            {
                ParserException.HandleRequirement(tok, ".");
                yield break;
            }
			rpn.AddRange(iterator.ParseEnd(state));

            foreach(Command c in rpn) yield return c;

            state.Labels[iterator.Label1_BreakLoop] = state.Count + 1;
            state.Labels[lbl2] = iterator.Label2Hop;
            state.Labels[iterator.Label3_NewIteration] = iterator.Label3Hop;
		}
	}
}