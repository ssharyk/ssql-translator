﻿using System;
using System.Collections.Generic;

namespace SSqlTranslator.Language.Parser
{
	/// <summary>
	/// Contains data required for handling current state of parsing
	/// </summary>
	public class ParsingState
	{
		#region Properties

		/// <summary>
		/// Gets index of token parser head points to
		/// </summary>
		public int CurrentTokenIndex { get; private set; }

		/// <summary>
		/// Gets global stack of operators and operands
		/// </summary>
		public Stack<string> OperatorsStack { get; private set; }

		/// <summary>
		/// Gets number of defined labels
		/// </summary>
		public int CurrentLabel { get; private set; }
		/// <summary>
		/// Gets map [Label number -> Command number]
		/// </summary>
		public Dictionary<int, int> Labels { get; private set; }

		/// <summary>
		/// Gets total number of parsed commands
		/// </summary>
		public int Count { get; set; }

		#endregion

		#region Construction

		public ParsingState()
		{
			this.OperatorsStack = new Stack<string>();
			this.Labels = new Dictionary<int, int>();
		}

		#endregion

		#region State changes

		public void Reset()
		{
			this.Count = 0;
			this.CurrentTokenIndex = 0;
			this.CurrentLabel = 0;
		}

		public int NextToken()
		{
			this.CurrentTokenIndex++;
			return this.CurrentTokenIndex;
		}

		public int PeekNextToken()
		{
			return CurrentTokenIndex + 1;
		}

		public int NextLabel()
		{
			this.CurrentLabel++;
			return this.CurrentLabel;
		}

		public int Back(int count)
		{
			this.CurrentTokenIndex -= count;
			return this.CurrentTokenIndex;
		}

		#endregion
	}
}
