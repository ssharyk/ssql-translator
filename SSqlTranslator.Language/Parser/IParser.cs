﻿using System;
using System.Collections.Generic;

using SSqlTranslator.Language.Parser.Commands;

namespace SSqlTranslator.Language.Parser
{
	/// <summary>
	/// Converts logic for converting script into Reverse Polish Notation
	/// </summary>
	public interface IParser
	{
		/// <summary>
		/// Executes parsing lexemes list into RPN
		/// </summary>
		/// <returns>Collection of RPN items for specific expression</returns>
		IEnumerable<Command> Parse();

		/// <summary>
		/// Gets current state of parsing process
		/// </summary>
		ParsingState State { get; }

		/// <summary>
		/// Gets full collection of RPN items to execute
		/// </summary>
		IEnumerable<Command> Commands { get; }
	}
}
