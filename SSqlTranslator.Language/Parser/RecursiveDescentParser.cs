﻿using System;
using System.Collections.Generic;
using System.Linq;

using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Parser.Strategies;
using SSqlTranslator.Language.Scaner;

namespace SSqlTranslator.Language.Parser
{
	/// <summary>
	/// Script parser based on Recursive descent parser approach
	/// </summary>
	public class RecursiveDescentParser : IParser
	{
		#region Fields and properties

		private readonly ITokenizer _scaner;
		private readonly ParsersFactory _parsersFactory;

		/// <summary>
		/// Gets current state of parsing process
		/// </summary>
		public ParsingState State { get; private set; }

		private List<Command> _commands;

		/// <summary>
		/// Gets full collection of RPN items to execute
		/// </summary>
		public IEnumerable<Command> Commands
		{
			get { return _commands; }
			private set { _commands = value.ToList(); }
		}

		#endregion

		#region Construction

		public RecursiveDescentParser(ITokenizer tokenizer)
		{
			this._scaner = tokenizer;
			this._parsersFactory = new ParsersFactory(_scaner);
			this.State = new ParsingState();

			this.Commands = new List<Command>();
		}

		#endregion

		#region Main methods

		/// <summary>
		/// Executes parsing lexemes list into RPN
		/// </summary>
		/// <returns>Collection of RPN items for specific expression</returns>
		public IEnumerable<Command> Parse()
		{
			State.Reset();
			for (; State.CurrentTokenIndex < _scaner.Lexemes.Count(); State.NextToken())
            {
                Lexeme token = _scaner.GetToken(State.CurrentTokenIndex);
                IParsingStrategy parser = _parsersFactory.Create(token);
	        	if (parser != null)
	        	{
	        		List<Command> commands = parser.Parse(State).ToList();
	        		_commands.AddRange(commands);
	        		State.Count = _commands.Count();
	        	}
            }

			_commands.Add(new EndOfProgramCommand());
            return _commands;
		}

		#endregion

		#region Action wrappers

		private void Add(Lexeme token)
		{
			Add(token.Value);
		}

		private void Add(string value)
		{
			_commands.Add(value);
		}

		#endregion
	}
}
