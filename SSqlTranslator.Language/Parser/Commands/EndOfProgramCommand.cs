﻿using System;

namespace SSqlTranslator.Language.Parser.Commands
{
	/// <summary>
	/// Represents symbol of program termination
	/// </summary>
	public class EndOfProgramCommand : Command
	{
		public EndOfProgramCommand()
			: base("$EOP")
		{
		}
	}
}
