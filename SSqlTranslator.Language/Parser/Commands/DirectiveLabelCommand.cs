﻿using System;

namespace SSqlTranslator.Language.Parser.Commands
{
	/// <summary>
	/// Represents item of RPN that is label for jumping in source (for ID, FOR, etc)
	/// </summary>
	public class DirectiveLabelCommand : Command
	{
		public int Number { get; private set; }

		private DirectiveLabelCommand(string val)
			: base(val)
		{
		}

		public DirectiveLabelCommand(int number)
			: this("@" + number)
		{
			this.Number = number;
		}
	}
}
