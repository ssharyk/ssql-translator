﻿using System;

namespace SSqlTranslator.Language.Parser.Commands
{
	/// <summary>
	/// Represents item of RPN with assignment symbol
	/// </summary>
	public class AssignmentCommand : Command
	{
		public AssignmentCommand()
			: base(":")
		{
		}
	}
}
