﻿using System;

namespace SSqlTranslator.Language.Parser.Commands
{
	/// <summary>
	/// Represents item of RPN
	/// </summary>
	public class Command
	{
		/// <summary>
		/// Gets or sets value of RPN item
		/// </summary>
		public string Value { get; private set; }

		#region Construction

		public Command(string value)
		{
			this.Value = value;
		}

		public static implicit operator Command(string command)
		{
			return new Command(command);
		}

		#endregion

		#region Equality

		public override bool Equals(object obj)
		{
			Command other = obj as Command;
			if (other == null)
				return false;
			return this.Value == other.Value;
		}

		public override int GetHashCode()
		{
			int hashCode = 0;
			unchecked
			{
				if (Value != null)
					hashCode += 1000000007 * Value.GetHashCode();
			}
			return hashCode;
		}

		public static bool operator ==(Command lhs, Command rhs)
		{
			if (ReferenceEquals(lhs, rhs))
				return true;
			if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
				return false;
			return lhs.Equals(rhs);
		}

		public static bool operator !=(Command lhs, Command rhs)
		{
			return !(lhs == rhs);
		}

		#endregion

		public override string ToString()
		{
			return string.Format("[Command Value={0}]", Value);
		}
	}
}
