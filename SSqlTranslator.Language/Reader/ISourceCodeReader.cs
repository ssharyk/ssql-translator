﻿using System;

namespace SSqlTranslator.Language.Reader
{
	/// <summary>
	/// Contains logic for getting source SSQL script code
	/// </summary>
	public interface ISourceCodeReader
	{
		/// <summary>
		/// Reads script
		/// </summary>
		/// <param name="src">Source of the script(plain text, file path, etc)</param>
		/// <returns>Source code of the script</returns>
		string Read(string src);
	}
}
