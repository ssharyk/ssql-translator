﻿using System;
using SSqlTranslator.Utilities.Helpers;

namespace SSqlTranslator.Language.Reader
{
	public class FileReader : ISourceCodeReader
	{
		/// <summary>
		/// Reads script
		/// </summary>
		/// <param name="path">Absolute or relative path to the file contains source of the script</param>
		/// <returns>Source code of the script</returns>
		public string Read(string path)
		{
			return FileUtils.Read(path);
		}
	}
}
