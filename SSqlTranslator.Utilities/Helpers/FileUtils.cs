﻿using System;
using System.IO;
using System.Text;

namespace SSqlTranslator.Utilities.Helpers
{
	/// <summary>
	/// Helper methods for file working
	/// </summary>
	public static class FileUtils
	{
		/// <summary>
		/// Reads file content
		/// </summary>
		/// <param name="path">Absolute or relative path to the file</param>
		/// <returns>File content</returns>
		public static string Read(string path)
		{
			string content = null;
			using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
			{
				using(StreamReader reader = new StreamReader(stream, Encoding.Default))
				{
					content = reader.ReadToEnd();
					reader.Close();
				}
				stream.Close();
			}
			return content;
		}

		/// <summary>
		/// Writes with specific content new file or overrides exisiting one
		/// </summary>
		/// <param name="path">Absolute or relative path to the file</param>
		/// <param name="content">Content to write to the file</param>
		public static void Write(string path, string content)
		{
			using(FileStream stream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
			{
				using(StreamWriter writer = new StreamWriter(stream, Encoding.Default))
				{
					writer.Write(content);
					writer.Close();
				}
				stream.Close();
			}
		}
	}
}
