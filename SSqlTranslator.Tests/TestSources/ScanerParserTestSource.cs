﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Parser.Strategies;
using SSqlTranslator.Language.Scaner;
using SSqlTranslator.Tests.Mocks;
using SSqlTranslator.Utilities.Helpers;

namespace SSqlTranslator.Tests.TestSources
{
	public struct ScanerParser
	{
		public ITokenizer Scaner { get; private set; }
		public string Rpn { get; private set; }
		public string FileName { get; private set; }
		public int StartTokenIndex { get; private set; }
		public IEnumerable<Command> PredefinedCommands {get; private set; }
		public Dictionary<int, int> Labels { get; private set; }

		public IEnumerable<Command> Commands =>
			Rpn.Split(' ').Select(c => new Command(c.Trim()));

		public IParsingStrategy Strategy
		{
			get
			{
				string name = Char.ToUpper(FileName[0]) + FileName.Substring(1).ToLower();
				int underscoreIndex = FileName.IndexOf('_');
				if (underscoreIndex != -1)
				{
					name = name.Substring(0, underscoreIndex);
				}
				name += "Strategy";
				string assemblyPath = Environment.CurrentDirectory + "\\SSqlTranslator.Language.dll";
	            Assembly assembly = Assembly.LoadFrom(assemblyPath);
	            Type type = assembly.GetType("SSqlTranslator.Language.Parser.Strategies." + name);
				return Activator.CreateInstance(type, Scaner) as IParsingStrategy;
			}
		}

		public ScanerParser(string fileName, ITokenizer scaner, string rpn, IEnumerable<Command> predefinedCommands, Dictionary<int, int> labels, int startIndex)
		{
			this.FileName = fileName;
			this.Scaner = scaner;
			this.Rpn = rpn;
			this.PredefinedCommands = predefinedCommands;
			this.StartTokenIndex = startIndex;
			this.Labels = labels;
		}
	}

	public static class ScanerParserTestSource
	{
		private const string SCRIPTS_RELATIVE_PATH = "../../Scripts/";
		private const string FILE_PREFIX = "parser_";
		private const string INPUT_FILE_EXTENSION = "ssql";
		private const string OUTPUT_FILE_EXTENSION = "ans";

		static ScanerParserTestSource()
		{
			ProgramsAndRpns = CreateTestCases();
		}

		public static IEnumerable<ScanerParser> ProgramsAndRpns {get; private set; }

		private static IEnumerable<ScanerParser> CreateTestCases()
		{
			List<ScanerParser> testCases = new List<ScanerParser>();
			string[] parserTasks = Directory.GetFiles(SCRIPTS_RELATIVE_PATH, FILE_PREFIX + "*." + INPUT_FILE_EXTENSION);
			for (int i = 0; i < parserTasks.Length; i++)
			{
				string answerFileName = parserTasks[i].Substring(
					SCRIPTS_RELATIVE_PATH.Length + FILE_PREFIX.Length,
					parserTasks[i].Length - (SCRIPTS_RELATIVE_PATH.Length + FILE_PREFIX.Length + 1 + INPUT_FILE_EXTENSION.Length));
				string fullAnswerFileName = $"{SCRIPTS_RELATIVE_PATH}{FILE_PREFIX}{answerFileName}.{OUTPUT_FILE_EXTENSION}";
				if (File.Exists(fullAnswerFileName))
				{
					ITokenizer scanerMock = ScanerMockFactory.CreateMock(fullAnswerFileName);
					string[] lines = FileUtils.Read(fullAnswerFileName).Split('\n');
					int startIndex = int.Parse(lines[lines.Length - 4]);
					string predef = lines[lines.Length - 3].Trim();
					IEnumerable<Command> predefinedCommands = (predef.Length > 0)
						? predef.Split(new String[] {" "}, StringSplitOptions.RemoveEmptyEntries).Select(c => new Command(c.Trim()))
						: new List<Command>();
					string rpn = lines[lines.Length - 2];
					int[] labelsPointers = lines.Last()
						.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries)
						.Select(l => int.Parse(l))
						.ToArray();
					Dictionary<int, int> labels = Enumerable
						.Range(0, labelsPointers.Length)
						.ToDictionary(ind => ind + 1, ind => labelsPointers[ind]);
					testCases.Add(new ScanerParser(answerFileName, scanerMock, rpn, predefinedCommands, labels, startIndex));
				}
			}
			return testCases;
		}
	}
}
