﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using NUnit.Framework;

using SSqlTranslator.Language.Parser;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Reader;
using SSqlTranslator.Language.Scaner;
using SSqlTranslator.Tests.TestSources;

namespace SSqlTranslator.Tests.Fixtures
{
	[TestFixture]
	public class ParserTests
	{
		[Test]
		[TestCaseSource(typeof(ScanerParserTestSource), nameof(ScanerParserTestSource.ProgramsAndRpns))]
		public void Parse_Valid_Success(ScanerParser testCase)
		{
			List<Command> commands = testCase.Commands.ToList();
			commands.Add(new EndOfProgramCommand());
			IParser parser = new RecursiveDescentParser(testCase.Scaner);
			TestDelegate action = () => parser.Parse();
			Assert.DoesNotThrow(action);
			CollectionAssert.AreEqual(commands, parser.Commands);
		}

		[Test]
		public void Parse_ValidMultiStatement_Success()
		{
			ISourceCodeReader reader = new FileReader();
			ITokenizer scaner = new Scaner(reader, "../../Scripts/parser.ssql");
			scaner.Scan();
			IParser parser = new RecursiveDescentParser(scaner);
			TestDelegate action = () => parser.Parse();
			Assert.DoesNotThrow(action);
			var expected =  GetTestCaseResult("../../Scripts/parser.ans");
			CollectionAssert.AreEqual(expected.Item1, parser.Commands);
			for(int i = 0; i < expected.Item2.Count; i++)
			{
				Console.WriteLine($"{expected.Item2.ElementAt(i)} -> {parser.State.Labels[i+1]}");
			}
			CollectionAssert.AreEqual(expected.Item2, parser.State.Labels.Values);
		}

		private Tuple<ICollection<Command>, ICollection<int>> GetTestCaseResult(string filePath)
		{
			ICollection<Command> commands = new List<Command>();
			ICollection<int> labels = new List<int>();
			using(FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
			{
				using(StreamReader reader = new StreamReader(stream, System.Text.Encoding.Default))
				{
					int rpnCount = int.Parse(reader.ReadLine());
					for (int i = 0; i < rpnCount; i++)
					{
						commands.Add(reader.ReadLine().Trim());
					}

					labels = reader.ReadLine()
						.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries)
						.Select(lS => int.Parse(lS))
						.ToList();
					reader.Close();
				}
				stream.Close();
			}
			return Tuple.Create(commands, labels);
		}
	}
}
