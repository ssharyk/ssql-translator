﻿using System;
using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

using SSqlTranslator.Language.Parser;
using SSqlTranslator.Language.Parser.Commands;
using SSqlTranslator.Language.Parser.Strategies;
using SSqlTranslator.Language.Scaner;
using SSqlTranslator.Tests.TestSources;

namespace SSqlTranslator.Tests.Fixtures
{
	[TestFixture]
	public class ParsingStrategiesTests
	{
		private static readonly ParsersFactory ParsersFactory =
			new ParsersFactory(null);

		[Test]
		[TestCase(LexemeType.Присваивание, ":", typeof(AssignmentStrategy))]
		[TestCase(LexemeType.КлючевоеСлово, "ЕСЛИ", typeof(IfStrategy))]
		[TestCase(LexemeType.КлючевоеСлово, "ВЫВОД", typeof(PrintStrategy))]
		[TestCase(LexemeType.КлючевоеСлово, "ДЛЯ", typeof(ForeachStrategy))]
		public void Strategy_Factory_Success(LexemeType lexemeType, string tokenValue, Type expectedParserType)
		{
			Lexeme lexeme = new Lexeme(lexemeType, tokenValue, 0, 0);
			IParsingStrategy strategy = null;
			TestDelegate action = () => strategy = ParsersFactory.Create(lexeme);
			Assert.DoesNotThrow(action);
			Assert.IsNotNull(strategy);
			Assert.IsInstanceOf(expectedParserType, strategy);
		}

		[Test]
		[TestCaseSource(typeof(ScanerParserTestSource), nameof(ScanerParserTestSource.ProgramsAndRpns))]
		public void Strategy_Parse_Success(ScanerParser testCase)
		{
			IParsingStrategy strategy = testCase.Strategy;
			ParsingState state = new ParsingState();
			for (int i = 0; i < testCase.StartTokenIndex; i++) state.NextToken();

			IEnumerable<Command> commands = null;
			Assert.DoesNotThrow(() => commands = strategy.Parse(state));
			Assert.IsNotNull(commands);

			List<Command> allCommands = new List<Command>();
			allCommands.AddRange(testCase.PredefinedCommands);
			allCommands.AddRange(commands);
			Assert.AreEqual(testCase.Commands.Count(), allCommands.Count());
			CollectionAssert.AreEqual(testCase.Commands, allCommands);
			CollectionAssert.AreEqual(testCase.Labels, state.Labels);
		}
	}
}
