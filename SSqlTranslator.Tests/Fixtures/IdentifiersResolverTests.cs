﻿using System;
using System.Collections.Generic;

using NUnit.Framework;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Reader;
using SSqlTranslator.Language.Scaner;
using SSqlTranslator.Tests.Mocks;

namespace SSqlTranslator.Tests.Fixtures
{
	[TestFixture]
	public class IdentifiersResolverTests
	{
		#region Fields and properties

		private const string SCRIPT_IDENTIFIERS_FILE_PATH = "../../Scripts/identifiers.ssql";
		private static readonly IList<Identifier> Identifiers = new List<Identifier>();

		private static readonly ISourceCodeReader MockReader = new ContentReaderMock();
		private static readonly ISourceCodeReader Reader = new FileReader();

		static IdentifiersResolverTests()
		{
			Identifiers.Add(new Identifier("_МАКС", DataTypes.ВЕЩ, "-10", false));
			Identifiers.Add(new Identifier(null, DataTypes.ВЕЩ, "0", true));
			Identifiers.Add(new Identifier(null, DataTypes.ВЕЩ, "10", true));
			Identifiers.Add(new Identifier("__ГРАВ", DataTypes.ВЕЩ, "6.67408", true));
			Identifiers.Add(new Identifier("__СОЛНЦЕ", DataTypes.ВЕЩ, "1.98892", true));
			Identifiers.Add(new Identifier("_ПЛ", DataTypes.СТР, "НЕОПРЕДЕЛЕНО!", false));
		}

		#endregion

		#region Tests

		[Test]
		[TestCase("#\n_x : 4;\n#")]
		[TestCase("#\n__x : 4;\n#")]
		[TestCase("#\n_x;\n#")]
		public void GetIdentifiers_NoTypeSpecified_Exception(string script)
		{
			ITokenizer scaner = new Scaner(MockReader, script);
			var exc = Assert.Throws(typeof(IdentifierException), () => scaner.Scan()) as IdentifierException;
			Assert.IsNotNull(exc);
			Assert.AreEqual(IdentifierExceptionReason.IdentifierTypeNotSpecified, exc.Reason);
		}

		[Test]
		public void GetIdentifiers_Valid_Success()
		{
			ITokenizer scaner = new Scaner(Reader, SCRIPT_IDENTIFIERS_FILE_PATH);
			scaner.Scan();
			CollectionAssert.AreEqual(Identifiers, scaner.Identifiers);
		}

		#endregion
	}
}
