﻿using System;
using System.IO;
using NUnit.Framework;

using SSqlTranslator.Language.Reader;
using SSqlTranslator.Utilities.Helpers;

namespace SSqlTranslator.Tests.Fixtures
{
	[TestFixture]
	public class ReaderTests
	{
		#region Input data and stubs

		private static readonly string AbsolutePath = "";
		private const string RELATIVE_PATH = "../../Scripts/read_from_file.ssql";
		private static readonly string[] Paths;

		private const string FILE_CONTENT =
@"#
СОЗДАТЬ ТАБЛИЦУ Т2 (ВЕЩ Ключ2, СТР Описание2).
#";

		#endregion

		#region SetUp/TearDown

		static ReaderTests()
		{
			string drive = Directory.Exists("D:\\") ? "D" : "C";
			string uid = Guid.NewGuid().ToString();
			AbsolutePath = $"{drive}:\\{uid}.ssql";

			Paths = new string[] { AbsolutePath, RELATIVE_PATH };
		}

		[SetUp]
		public void CreateMockFile()
		{
			if (File.Exists(AbsolutePath))
			{
				return;
			}
			FileUtils.Write(AbsolutePath, FILE_CONTENT);
		}

		[TearDown]
		public void RemoveMockFile()
		{
			if (!File.Exists(AbsolutePath))
			{
				return;
			}
			File.Delete(AbsolutePath);
		}

		#endregion

		#region Test fixtures

		[Test]
		[TestCaseSource(nameof(ReaderTests.Paths))]
		public void Read_FileExists_Success(string path)
		{
			ISourceCodeReader reader = new FileReader();
			string content = null;
			TestDelegate readingProcess = () => content = reader.Read(path);
			Assert.DoesNotThrow(readingProcess);
			Assert.IsNotNull(content);
			Assert.AreEqual(FILE_CONTENT, content);
		}

		[Test]
		[TestCaseSource(nameof(ReaderTests.Paths))]
		public void Read_FileDoesNotExists_Exception(string path)
		{
			ISourceCodeReader reader = new FileReader();
			TestDelegate readingProcess = () => reader.Read(path + ".inv");
			Assert.Throws(typeof(FileNotFoundException), readingProcess);
		}

		#endregion
	}
}
