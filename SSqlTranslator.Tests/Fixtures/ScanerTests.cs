﻿using System;
using System.Collections.Generic;

using NUnit.Framework;

using SSqlTranslator.Language.Infrastructure;
using SSqlTranslator.Language.Reader;
using SSqlTranslator.Language.Scaner;
using SSqlTranslator.Tests.Mocks;

namespace SSqlTranslator.Tests.Fixtures
{
	[TestFixture]
	public class ScanerTests
	{
		#region Fields and properties

		private static readonly ISourceCodeReader Reader = new FileReader();
		private static readonly ISourceCodeReader MockReader = new ContentReaderMock();

		private const string SCRIPT_FILE_PATH = "../../Scripts/scaner.ssql";
		private static readonly IList<Lexeme> Lexemes = new List<Lexeme>();

		static ScanerTests()
		{
			Lexemes.Add(new Lexeme(LexemeType.КлючевоеСлово, "СОЗДАТЬ", 2, 1));
			Lexemes.Add(new Lexeme(LexemeType.КлючевоеСлово, "ТАБЛИЦУ", 2, 9));
			Lexemes.Add(new Lexeme(LexemeType.ИмяТаблицы, "Т2", 2, 17));
			Lexemes.Add(new Lexeme(LexemeType.Разделитель, "(", 2, 20));

			Lexemes.Add(new Lexeme(LexemeType.КлючевоеСлово, "ВЕЩ", 3, 5));
			Lexemes.Add(new Lexeme(LexemeType.ИмяПоля, "КЛЮЧ2", 3, 9));
			Lexemes.Add(new Lexeme(LexemeType.Разделитель, ",", 3, 14));

			Lexemes.Add(new Lexeme(LexemeType.КлючевоеСлово, "СТР", 4, 5));
			Lexemes.Add(new Lexeme(LexemeType.ИмяПоля, "ОПИСАНИЕ2", 4, 9));

			Lexemes.Add(new Lexeme(LexemeType.Разделитель, ")", 5, 1));
			Lexemes.Add(new Lexeme(LexemeType.КонецВыражения, ".", 5, 2));
		}

		#endregion

		#region Tests

		[Test]
		public void Create_ValidSource_Success()
		{
			ITokenizer scaner = null;
			Assert.DoesNotThrow(() => scaner = new Scaner(Reader, SCRIPT_FILE_PATH));
			Assert.IsNotNull(scaner);
		}

		[Test]
		public void Create_InvalidSource_Exception()
		{
			ITokenizer scaner = null;
			TestDelegate constructor = () => scaner = new Scaner(Reader, SCRIPT_FILE_PATH + ".inv");
			var scanerExc = Assert.Throws(typeof(ScanerException), constructor) as ScanerException;
			Assert.IsNotNull(scanerExc);
			Assert.AreEqual(ScanerExceptionReason.InvalidSource, scanerExc.Reason);
			Assert.IsNull(scaner);
		}

		[Test]
		[TestCase("", ScanerExceptionReason.InvalidTransactionBounds)]
		[TestCase("#\nСОЗДАТЬ ТАБЛИЦУ", ScanerExceptionReason.InvalidTransactionBounds)]
		[TestCase("#\nСОЗДАТЬ ТАБЛИЦУ.", ScanerExceptionReason.InvalidTransactionBounds)]
		[TestCase("СОЗДАТЬ ТАБЛИЦУ", ScanerExceptionReason.InvalidTransactionBounds)]
		[TestCase("СОЗДАТЬ ТАБЛИЦУ\n#", ScanerExceptionReason.InvalidTransactionBounds)]

		[TestCase("#\n??dfasdf\nfadfasf\n#", ScanerExceptionReason.InfiniteComment)]
		[TestCase("#\n\"dfasdf\nfadfasf\n#", ScanerExceptionReason.InfiniteStringLiteral)]

		[TestCase("#\nif (X = Y) {}\n#", ScanerExceptionReason.InvalidComparison)]
		public void Scan_Invalid_Exception(string content, ScanerExceptionReason expectedReason)
		{
			ITokenizer scaner = new Scaner(MockReader, content);
			TestDelegate constructor = () => scaner.Scan();
			var scanerExc = Assert.Throws(typeof(ScanerException), constructor) as ScanerException;
			Assert.IsNotNull(scanerExc);
			Assert.AreEqual(expectedReason, scanerExc.Reason);
		}

		[Test]
		public void Scan_Valid_Success()
		{
			ITokenizer scaner = new Scaner(Reader, SCRIPT_FILE_PATH);
			int lexemesCount = 0;
			TestDelegate action = () => lexemesCount = scaner.Scan();
			Assert.DoesNotThrow(action);
			Assert.AreEqual(Lexemes.Count, lexemesCount);
			CollectionAssert.AreEqual(Lexemes, scaner.Lexemes);
		}
		#endregion
	}
}
