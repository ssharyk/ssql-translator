﻿using System;

using System.Collections.Generic;
using Moq;

using SSqlTranslator.Language.Scaner;
using SSqlTranslator.Utilities.Helpers;

namespace SSqlTranslator.Tests.Mocks
{
	/// <summary>
	/// Mock for scaner that parses lines to lexemes
	/// </summary>
	public static class ScanerMockFactory
	{
		/// <summary>
		/// Creates new instance of mocked <see cref="ITokenizer"/> using plain list lexemes representation
		/// </summary>
		/// <returns></returns>
		public static ITokenizer CreateMock(string filePath)
		{
			string fileContent = FileUtils.Read(filePath);
			string[] lines = fileContent.Split('\n');

			List<Lexeme> lexemes = new List<Lexeme>();
			int lexemesNumber = int.Parse(lines[0]);
			for (int i = 1; i <= lexemesNumber; i++)
			{
				lexemes.Add(ParseLexeme(lines[i].Trim()));
			}

			List<Identifier> identifiers = new List<Identifier>();
			int identifiersNumber = int.Parse(lines[lexemesNumber + 1]);
			for (int i = lexemesNumber + 2; i <= lexemesNumber + identifiersNumber + 1; i++)
			{
				identifiers.Add(ParseIdentifier(lines[i].Trim()));
			}

			Mock<ITokenizer> scanerMock = new Mock<ITokenizer>();
			scanerMock.SetupGet(sc => sc.Lexemes).Returns(lexemes);
			scanerMock.SetupGet(sc => sc.Identifiers).Returns(identifiers);
			scanerMock.Setup(sc => sc.GetToken(It.IsAny<int>())).Returns<int>(i => lexemes[i]);
			return scanerMock.Object;
		}

		/// <summary>
		/// Parses single lexeme in text file
		/// </summary>
		/// <param name="lexemeLine">String representation of lexeme in format: <see cref="DataTypes"/><code>LexemeType; string Value</code></param>
		private static Lexeme ParseLexeme(string lexemeLine)
		{
			string[] items = lexemeLine.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);
			LexemeType type = (LexemeType) Enum.Parse(typeof(LexemeType), items[0]);
			Lexeme lex = new Lexeme(type, items[1], 0, 0);
			lex.IdentifierReference = int.Parse(items[2]);
			return lex;
		}

		private static Identifier ParseIdentifier(string identifierLine)
		{
			string[] items = identifierLine.Split(' ');
			DataTypes type = (DataTypes) Enum.Parse(typeof(DataTypes), items[0]);
			bool isConst = items[3] == "Yes";
			return new Identifier(items[1], type, items[2], isConst);
		}
	}
}
