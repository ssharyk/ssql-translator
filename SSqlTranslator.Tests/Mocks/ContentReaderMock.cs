﻿using System;
using SSqlTranslator.Language.Reader;

namespace SSqlTranslator.Tests.Mocks
{
	/// <summary>
	/// Mock for source code reader that returns exactly the same string was passed
	/// </summary>
	public class ContentReaderMock : ISourceCodeReader
	{
		public string Read(string src)
		{
			return src;
		}
	}
}
